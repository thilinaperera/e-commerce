<section>
    <h2>Issued items</h2>
    <?php echo anchor('admin/issued/edit','<i class="glyphicon glyphicon-plus"></i> Add issued'); ?>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Product</th>
            <th>Stock</th>
            <th>Date issued</th>
            <th>Quantity</th>
            <th>Expiry Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($issueds);
            if(count($issueds)){
                foreach($issueds as $issued){ ?>
                    <tr>
                        <td><?php echo anchor('admin/issued/edit/'.$issued->id, $issued->id) ?></td>
                        <td><?php echo $issued->product_id ?></td>
                        <td><?php echo $issued->stock_id ?></td>
                        <td><?php echo $issued->date_issued ?></td>
                        <td><?php echo $issued->quantity_issued ?></td>
                        <td><?php echo $issued->expiry_date ?></td>
                        <td><?php echo btn_edit('admin/issued/edit/'.$issued->id) ?></td>
                        <td><?php echo btn_delete('admin/issued/delete/'.$issued->id) ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>