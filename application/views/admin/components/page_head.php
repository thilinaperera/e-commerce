<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $meta_title ?></title>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,800italic,400,600,700,800' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/datepicker.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/dashboard.css'); ?>" rel="stylesheet">

    <style>
        body{
            font-family: 'Open Sans', sans-serif !important;
        }
        footer {
            color: #666;
            padding: 17px 0 18px 0;
            border-top: 1px solid #000;
            width: 100%;
            bottom: 0;
        }
        footer a {
            color: #999;
        }
        footer .container p{
            margin-bottom: 0px;
        }
        footer a:hover {
            color: #efefef;
        }
        .wrapper {
            min-height: 100%;
            height: auto !important;
            height: 100%;
            margin: 0 auto -63px;
        }
        .push {
            height: 63px;
        }
        .marginAdded{
            margin-bottom: 60px !important;
        }
        .nopadding {
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .addtoHolder
        {
            margin-top: 15px;
        }
    </style>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->

    <script src="<?php echo base_url('js/Chart.js'); ?>"></script>
</head>