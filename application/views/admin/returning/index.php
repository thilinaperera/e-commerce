<section>
    <h2>Returned Items</h2>
    <p><?php echo anchor('admin/returning/stock','<i class="glyphicon glyphicon-plus"></i> Add Stock returning items'); ?>
     <span> / </span>
    <?php echo anchor('admin/returning/issued','<i class="glyphicon glyphicon-plus"></i> Add Issued returning items'); ?>
    </p>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Product</th>
            <th>Stock</th>
            <th>Date</th>
            <th>Issued</th>
            <th>Quantity</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($returns);
            if(count($returns)){
                foreach($returns as $return){ ?>
                    <tr>
                        <td><?php
                            if($return->stock_id == 0){
                                echo anchor('admin/returning/issued/'.$return->id, $return->id);
                            }
                            else{
                                echo anchor('admin/returning/stock/'.$return->id, $return->id);
                            }
                            ?>
                        </td>
                        <td><?php echo $return->product ?></td>
                        <td><?php
                            if($return->stock_id == 0){
                                echo '-';
                            }
                            else{
                                echo $return->stock_id;
                            }

                            ?>
                        </td>
                        <td><?php echo $return->date?></td>
                        <td><?php
                            if($return->issued_id == 0){
                                echo '-';
                            }
                            else{
                                echo $return->issued_id;
                            }

                            ?>
                        </td>
                        <td><?php echo $return->quantity ?></td>
                        <td><?php
                            if($return->stock_id == 0){
                                echo btn_edit('admin/returning/issued/'.$return->id);
                            }
                            else{
                                echo btn_edit('admin/returning/stock/'.$return->id);
                            }
                            ?>

                        </td>
                        <td><?php echo btn_delete('admin/issued/delete/'.$return->id) ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>