<div class="modal-header">
    <h2 class="modal-title text-left"><?php echo empty($returning->id) ? 'Add a returning form stock' : 'Edit returning' ?></h2>
</div>
<div class="modal-body">
    <?php echo validation_errors() ?>
    <?php echo form_open('',array( 'class' => 'form-horizontal','role' => 'form' )) ?>
    <div class="form-group">
        <label for="stock_id" class="col-sm-2 control-label">Stock</label>
        <div class="col-sm-10">
            <?php echo form_dropdown('stock_id',$stocks,$this->input->post('stock_id') ? $this->input->post('stock_id') : $returning->stock_id,'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="date_issued" class="col-sm-2 control-label">Date</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'date',
                'type' => 'text',
                'name' => 'date',
                'class' => 'form-control datepicker',
                'placeholder' => ''
            ),set_value('date',$returning->date)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="quantity_issued" class="col-sm-2 control-label">Quantity</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'quantity',
                'type' => 'text',
                'name' => 'quantity',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('quantity',$returning->quantity)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="expiry_date" class="col-sm-2 control-label">Comment</label>
        <div class="col-sm-10">
            <?php echo form_textarea(array(
                'id' => 'comment',
                'type' => 'text',
                'name' => 'comment',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('comment',$returning->comment)) ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
    <?php echo form_close() ?>
</div>
<script>
    $(document).ready(
        function(){
            $('.datepicker').datepicker({
                format : 'yyyy-mm-dd'
            })
        }
    )
</script>