<?php $this->load->view('admin/components/page_head.php') ?>
<body>
<nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="color: #FFFFFF" href="<?php echo site_url('admin/dashboard'); ?>"><?php echo $meta_title ?></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
                <li><?php echo anchor('admin/user','Users'); ?></li>
                <li><?php echo anchor('admin/product','Products'); ?></li>
                <li><?php echo anchor('admin/category','Categories'); ?></li>
                <li><?php echo anchor('admin/brand','Brands'); ?></li>
                <li><?php echo anchor('admin/supplier','Suppliers'); ?></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Stocks <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><?php echo anchor('admin/stock','Stocks'); ?></li>
                        <li><?php echo anchor('admin/issued','Issued'); ?></li>
                        <li class="divider"></li>
                        <li><?php echo anchor('admin/returning','Returned'); ?></li>
                        <li><?php echo anchor('admin/returning/stock','Stock Returning'); ?></li>
                        <li><?php echo anchor('admin/returning/issued','Issued Returning'); ?></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><?php echo anchor('admin/report/users','Users'); ?></li>
                        <li><?php echo anchor('admin/report/returns','Returns'); ?></li>
                        <li><?php echo anchor('admin/report/orders','orders'); ?></li>
                        <li><?php echo anchor('admin/report/suppliers','Suppliers'); ?></li>
                        <li><?php echo anchor('admin/report/issued','Issued'); ?></li>
                    </ul>
                </li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><?php echo anchor('','<i class="glyphicon glyphicon-user"></i> '.$this->session->userdata("name").'') ?></li>
                <li> <?php echo anchor('admin/user/logout','<i class="glyphicon glyphicon-log-out"></i> logout') ?></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container marginAdded">
    <?php if($this->session->flashdata('error')){ ?>
        <div class="alert alert-danger fade in">
            <p><?php echo $this->session->flashdata('error'); ?></p>
        </div>
    <?php } ?>
    <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success fade in">
            <p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
    <?php } ?>

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?php $this->load->view($subview) ?>
        </div>
    </div>
</div>
<?php $this->load->view('admin/components/page_footer.php') ?>
<script>
    $(document).ready(function(){
        $(".alert").alert();
        setTimeout(function(){
            $(".alert").alert('close');
        }, 3000);

    })
</script>
