<section>
    <h2>Products</h2>
    <?php echo anchor('admin/product/edit','<i class="glyphicon glyphicon-plus"></i> Add product'); ?>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Category</th>
            <th>Brand</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($products);
            if(count($products)){
                foreach($products as $product){ ?>
                    <tr>
                        <td><?php echo anchor('admin/product/edit/'.$product->id, $product->name) ?></td>
                        <td><?php echo $product->description ?></td>
                        <td><?php echo $product->category ?></td>
                        <td><?php echo $product->brand ?></td>
                        <td><?php echo btn_edit('admin/product/edit/'.$product->id) ?></td>
                        <td><?php echo btn_delete('admin/product/delete/'.$product->id) ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>