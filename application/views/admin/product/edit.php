<div class="modal-header">
    <h2 class="modal-title text-left"><?php echo empty($product->id) ? 'Add a new product' : 'Edit product' ?></h2>
</div>
<div class="modal-body">
    <?php echo validation_errors() ?>
    <?php echo form_open_multipart('',array( 'class' => 'form-horizontal','role' => 'form' )) ?>
    <div class="form-group">
        <label for="category_id" class="col-sm-2 control-label">Category</label>
        <div class="col-sm-10">
            <?php echo form_dropdown('category_id',$categories,$this->input->post('category_id') ? $this->input->post('category_id') : $product->category_id,'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="brand_id" class="col-sm-2 control-label">Brand</label>
        <div class="col-sm-10">
            <?php echo form_dropdown('brand_id',$brands,$this->input->post('brand_id') ? $this->input->post('brand_id') : $product->brand_id,'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'name',
                'type' => 'text',
                'name' => 'name',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('name',$product->name)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'description',
                'type' => 'text',
                'name' => 'description',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('description',$product->description)) ?>
        </div>
    </div>
    <?php
    if($product->image != ''){ ?>
    <div class="form-group">
        <label for="image" class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-sm-10">

                <?php echo '<div class="alert alert-info">Product image already exist. <a href="'.site_url('uploads').'/'.$product->image.'" class="alert-link">Click here</a> to view </div>'; ?>

        </div>
    </div> <?php
    }
    ?>
    <div class="form-group">
        <label for="image" class="col-sm-2 control-label">Image</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'image',
                'type' => 'file',
                'name' => 'image',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('image',$product->image)) ?>
        </div>
    </div>
    <div class="form-group hidden">
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'image_check',
                'type' => 'hidden',
                'name' => 'image_check',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('image_check',$product->image)) ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
    <?php echo form_close() ?>
</div>