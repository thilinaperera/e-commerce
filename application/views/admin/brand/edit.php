<div class="modal-header">
    <h2 class="modal-title text-left"><?php echo empty($brand->id) ? 'Add a new brand' : 'Edit brand' ?></h2>
</div>
<div class="modal-body">
    <?php echo validation_errors() ?>
    <?php echo form_open('',array( 'class' => 'form-horizontal','role' => 'form' )) ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'name',
                'type' => 'text',
                'name' => 'name',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('name',$brand->name)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'description',
                'type' => 'text',
                'name' => 'description',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('description',$brand->description)) ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
    <?php echo form_close() ?>
</div>