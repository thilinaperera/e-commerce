<section>
    <h2>brands</h2>
    <?php echo anchor('admin/brand/edit','<i class="glyphicon glyphicon-plus"></i> Add brand'); ?>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($brands);
            if(count($brands)){
                foreach($brands as $brand){ ?>
                    <tr>
                        <td><?php echo anchor('admin/brand/edit/'.$brand->id, $brand->name) ?></td>
                        <td><?php echo $brand->description ?></td>
                        <td><?php echo btn_edit('admin/brand/edit/'.$brand->id) ?></td>
                        <td><?php echo btn_delete('admin/brand/delete/'.$brand->id) ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>