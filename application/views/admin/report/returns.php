<section>
    <h2>Returned Items Report</h2>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Product</th>
            <th>Stock</th>
            <th>Date</th>
            <th>Issued</th>
            <th>Quantity</th>
        </tr>
        </thead>
        <tbody>
        <?php
        //var_dump($returns);
        if(count($returns)){
            foreach($returns as $return){ ?>
                <tr>
                    <td><?php
                        if($return->stock_id == 0){
                            echo anchor('admin/returning/issued/'.$return->id, $return->id);
                        }
                        else{
                            echo anchor('admin/returning/stock/'.$return->id, $return->id);
                        }
                        ?>
                    </td>
                    <td><?php echo $return->product ?></td>
                    <td><?php
                        if($return->stock_id == 0){
                            echo 'No';
                        }
                        else{
                            echo $return->stock_id;
                        }

                        ?>
                    </td>
                    <td><?php echo $return->date?></td>
                    <td><?php
                        if($return->issued_id == 0){
                            echo 'No';
                        }
                        else{
                            echo $return->issued_id;
                        }

                        ?>
                    </td>
                    <td><?php echo $return->quantity ?></td>

                </tr>
            <?php
            }
        }
        else{ ?>
            <tr>
                <td>No records found</td>
            </tr>
        <?php
        }
        ?>




        </tbody>
    </table>
</section>