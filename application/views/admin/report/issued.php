<section>
    <h2>Issues</h2>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Stock</th>
            <th>Product</th>
            <th>Description</th>
            <th>Expiry Date</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($issueds);
            if(count($issues)){
                foreach($issues as $issued){ ?>
                    <tr>
                        <td><?php echo $issued->id ?></td>
                        <td><?php echo $issued->stock_id ?></td>
                        <td><?php echo $issued->name  ?></td>
                        <td><?php echo $issued->description  ?></td>
                        <td><?php echo $issued->expiry_date  ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>