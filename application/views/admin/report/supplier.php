<section>
    <h2>Suppliers</h2>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($suppliers);
            if(count($suppliers)){
                foreach($suppliers as $supplier){ ?>
                    <tr>
                        <td><?php echo $supplier->id ?></td>
                        <td><?php echo $supplier->name ?></td>
                        <td><?php echo $supplier->email  ?></td>
                        <td><?php echo $supplier->phone  ?></td>
                        <td><?php echo $supplier->description  ?></td>

                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>