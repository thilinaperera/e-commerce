<section>
    <h2>Users</h2>
    <?php echo anchor('admin/user/edit','<i class="glyphicon glyphicon-plus"></i> Add user'); ?>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Email</th>
            <th>Role</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($this->session->userdata);
            if(count($users)){
                foreach($users as $user){ ?>
                    <tr>
                        <td><?php echo anchor('admin/user/edit/'.$user->id, $user->email) ?></td>
                        <td><?php echo $user->usertype ?></td>
                        <td><?php echo btn_edit('admin/user/edit/'.$user->id) ?></td>
                        <td><?php  echo btn_delete('admin/user/delete/'.$user->id) ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</section>
