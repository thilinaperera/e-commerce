<div class="modal-header">
    <h2 class="modal-title text-left"><?php echo empty($page->id) ? 'Add a new page' : 'Edit page' ?></h2>
</div>
<div class="modal-body">
    <?php echo validation_errors() ?>
    <?php echo form_open('',array( 'class' => 'form-horizontal','role' => 'form' )) ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Parent</label>
        <div class="col-sm-10">
            <?php echo form_dropdown('parent_id',$pages_no_parents,$this->input->post('parent_id') ? $this->input->post('parent_id') : $page->parent_id,'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Title</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'title',
                'type' => 'text',
                'name' => 'title',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('title',$page->title)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Slug</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'slug',
                'type' => 'text',
                'name' => 'slug',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('slug',$page->slug)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-2 control-label">Order</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'order',
                'type' => 'text',
                'name' => 'order',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('order',$page->order)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="password_confirm" class="col-sm-2 control-label">Body</label>
        <div class="col-sm-10">
            <?php echo form_textarea(array(
                'id' => 'body',
                'type' => 'text',
                'name' => 'body',
                'class' => 'form-control tinymce',
                'placeholder' => ''
            ),set_value('body',$page->body)) ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
    <?php echo form_close() ?>
</div>