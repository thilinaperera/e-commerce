<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title text-center">Please Login</h4>
</div>
<div class="modal-body">
    <?php // echo '<pre>'.print_r($this->session->userdata,true).'</pre>' ; ?>
    <div class="well">
        <?php $attributes = array('class'=>'form-signin','role'=>'form') ?>
        <?php echo form_open('',$attributes) ?>
            <?php if(form_error('email')) { ?>
            <div class="form-group has-error">
                <label class="control-label" for="email"><?php echo form_error('email')  ?></label>
                <?php
                echo form_input(array(
                     'id' => 'email',
                     'type' => 'email',
                     'name' => 'email',
                     'class' => 'form-control',
                     'placeholder' => 'Email address'
                )) ?>
            </div>
            <?php }else{ ?>
                <div class="form-group">
                    <?php echo form_input(array(
                        'id' => 'email',
                        'type' => 'email',
                        'name' => 'email',
                        'class' => 'form-control',
                        'placeholder' => 'Email address'
                    )) ?>
                </div>
            <?php } ?>


           <?php if(form_error('password')) { ?>

            <div class="form-group has-error">
                <label class="control-label" for="email"><?php echo form_error('password') ?></label>
                <?php echo form_input(array(
                    'id' => 'password',
                    'type' => 'password',
                    'name' => 'password',
                    'class' => 'form-control',
                    'placeholder' => 'Password'
                )) ?>
            </div>

            <?php } else{?>
            <div class="form-group">
                 <?php echo form_input(array(
                     'id' => 'password',
                     'type' => 'password',
                     'name' => 'password',
                     'class' => 'form-control',
                     'placeholder' => 'Password'
                 )) ?>
            </div>
            <?php } ?>
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
            </label>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

        <?php echo form_close() ?>
    </div>
</div>