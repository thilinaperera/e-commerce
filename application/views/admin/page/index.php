<section>
    <h2>Pages</h2>
    <?php echo anchor('admin/page/edit','<i class="glyphicon glyphicon-plus"></i> Add page'); ?>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Title</th>
            <th>Parent</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($pages);
            if(count($pages)){
                foreach($pages as $page){ ?>
                    <tr>
                        <td><?php echo anchor('admin/page/edit/'.$page->id, $page->title) ?></td>
                        <td><?php echo $page->parent_slug ?></td>
                        <td><?php echo btn_edit('admin/page/edit/'.$page->id) ?></td>
                        <td><?php echo btn_delete('admin/page/delete/'.$page->id) ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>