<h2>Welcome back <span class="label label-success"><?php echo $this->session->userdata("name") ?></span></h2>
<br/>
<br/>
<div class="row">
    <div class="col-sm-6 col-md-6 col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading text-center">
                <h3 class="panel-title">Last month product issuing - ( <?php echo date('Y-m-01').' - '.date('Y-m-d') ?> )</h3>
            </div>
            <div class="panel-body text-center">
                <canvas id="myChart" width="500" height="300"></canvas>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading text-center">
                <h3 class="panel-title">Last month returned - ( <?php echo date('Y-m-01').' - '.date('Y-m-d') ?> )</h3>
            </div>
            <div class="panel-body text-center">
                <canvas id="myChart2" width="500" height="300"></canvas>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6">



        <div class="panel panel-primary">
            <div class="panel-heading text-center">
                <h3 class="panel-title">Status</h3>
            </div>
            <div class="panel-body text-center">
                <div id="big_stats" class="cf">
                    <div class="stat"> <span>Pending Orders</span>  <i class="glyphicon glyphicon-import"></i> <span class="value"><?php echo count($pending) ; ?></span> </div>
                    <!-- .stat -->

                    <div class="stat"> <span>Users</span><i class="glyphicon glyphicon-user"></i> <span class="value"><?php echo count ($users)  ?></span> </div>
                    <!-- .stat -->

                    <div class="stat"> <span>Products</span><i class="glyphicon glyphicon-list-alt"></i> <span class="value"><?php echo count ($products)  ?></span> </div>
                    <!-- .stat -->

                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading text-center">
                <h3 class="panel-title">Important Shortcuts</h3>
            </div>
            <div class="panel-body text-center">
                <div class="shortcuts">
                    <?php echo anchor('admin/product','<i class="shortcut-icon glyphicon glyphicon-list-alt"></i><span class="shortcut-label">Products</span>','class="shortcut"') ?>
                    <?php echo anchor('admin/user','<i class="shortcut-icon glyphicon glyphicon-user"></i><span class="shortcut-label">Users</span>','class="shortcut"') ?>
                    <?php echo anchor('admin/category','<i class="shortcut-icon glyphicon glyphicon-tasks"></i><span class="shortcut-label">Categories</span>','class="shortcut"') ?>
                    <?php echo anchor('admin/brand','<i class="shortcut-icon glyphicon glyphicon-thumbs-up"></i><span class="shortcut-label">Brands</span>','class="shortcut"') ?>
                    <?php echo anchor('admin/stock','<i class="shortcut-icon glyphicon glyphicon-th"></i><span class="shortcut-label">Stocks</span>','class="shortcut"') ?>
                    <?php echo anchor('admin/issued','<i class="shortcut-icon glyphicon glyphicon-ok"></i><span class="shortcut-label">Issued</span>','class="shortcut"') ?>
                </div>
                <!-- /shortcuts -->
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="list-group">
                <a href="#" class="list-group-item ">
                    <span class="badge"><?php echo count($pending); ?></span>
                    Pending Orders
                </a>
                <a href="#" class="list-group-item active text-center">Latest Order Details</a>
                <?php
                if(count($pending) != 0 ){
                    $products_cart = $this->shopping_m->get_last_pending_order();
                    //var_dump($products_cart);
                    $order_id = null;
                    foreach($products_cart as $item){
                        $order_id = $item['order_id'];
                        ?>

                        <a href="#" class="list-group-item">
                            <span class="badge"><?php  echo $item['qty'] ?></span>
                            <?php echo $item['name'] ?>
                        </a>

                    <?php
                    }

                    ?>
                    <?php echo form_open('admin/order/set/'.$order_id,array( 'class' => 'form-horizontal','role' => 'form' )) ?>
                    <a href="#" class="list-group-item ">
                        <?php echo form_dropdown('status',array('complete' => 'complete','rejected' => 'reject'),'','class="form-control"') ?>
                    </a>
                    <a href="#" class="list-group-item ">
                        <button type="submit" class="btn btn-success btn-lg btn-block">Respond</button>

                    </a>
                    <?php form_close();
                }else{ ?>
                    <a href="#" class="list-group-item">
                        No pending orders
                    </a>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
//var_dump($issued_chart);
$dates_issued = array();
$issued_quantities = array();
foreach($issued_chart as $item){
   array_push($dates_issued,$item->date_issued) ;
   array_push($issued_quantities,(int)$item->quantity_issued) ;
}
$dates_issued = json_encode($dates_issued);
$issued_quantities = json_encode($issued_quantities);
?>

<?php
//var_dump($issued_chart);
$dates = array();
$returned_quantities = array();
foreach($returns as $x){
    //var_dump($x->date);
    array_push($dates,$x->date) ;
    array_push($returned_quantities,(int)$x->quantity) ;
}
$dates = json_encode($dates);
$returned_quantities = json_encode($returned_quantities);

?>
<script>

var data = {
    labels : <?php echo $dates_issued ?>,
    datasets : [
        {
            fillColor : "rgba(151,187,205,0.5)",
            strokeColor : "rgba(151,187,205,1)",
            pointColor : "rgba(151,187,205,1)",
            pointStrokeColor : "#fff",
            data : <?php echo $issued_quantities ?>
        }
    ]
}


var data2 = {
    labels : <?php echo $dates ?>,
    datasets : [
        {
            fillColor : "rgba(151,187,205,0.5)",
            strokeColor : "rgba(151,187,205,1)",
            pointColor : "rgba(151,187,205,1)",
            pointStrokeColor : "#fff",
            data : <?php echo $returned_quantities ?>
        }
    ]
}

var ctx = $("#myChart").get(0).getContext("2d");
var ctx2 = $("#myChart2").get(0).getContext("2d");
var myNewChart =new Chart(ctx).Line(data,{scaleShowLabels : true, pointLabelFontSize : 14});
var myNewChart2 =new Chart(ctx2).Line(data2,{scaleShowLabels : true, pointLabelFontSize : 14});
</script>
