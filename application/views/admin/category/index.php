<section>
    <h2>Categories</h2>
    <?php echo anchor('admin/category/edit','<i class="glyphicon glyphicon-plus"></i> Add category'); ?>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($categorys);
            if(count($categories)){
                foreach($categories as $category){ ?>
                    <tr>
                        <td><?php echo anchor('admin/category/edit/'.$category->id, $category->name) ?></td>
                        <td><?php echo $category->description ?></td>
                        <td><?php echo btn_edit('admin/category/edit/'.$category->id) ?></td>
                        <td><?php echo btn_delete('admin/category/delete/'.$category->id) ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>