<div class="modal-header">
    <h2 class="modal-title text-left"><?php echo empty($issued->id) ? 'Add a new issued' : 'Edit issued' ?></h2>
</div>
<div class="modal-body">
    <?php echo validation_errors() ?>
    <?php echo form_open('',array( 'class' => 'form-horizontal','role' => 'form' )) ?>
    <?php
    if( !empty($issued->id) ){
        $array = array(
            $issued->stock_id => $issued->stock_id
        )
        ?>
        <div class="form-group">
            <label for="stock_id" class="col-sm-2 control-label">Stock</label>
            <div class="col-sm-10">
                <?php echo form_dropdown('stock_id', $array ,$issued->stock_id,'class="form-control"') ?>
            </div>
        </div>
    <?php
    }else{ ?>

        <div class="form-group">
            <label for="stock_id" class="col-sm-2 control-label">Stock</label>
            <div class="col-sm-10">
                <?php echo form_dropdown('stock_id',$stocks,$this->input->post('stock_id') ? $this->input->post('stock_id') : $issued->stock_id,'class="form-control"') ?>
            </div>
        </div>
    <?php
    }
    ?>

    <div class="form-group">
        <label for="date_issued" class="col-sm-2 control-label">Date issued</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'date_issued',
                'type' => 'text',
                'name' => 'date_issued',
                'class' => 'form-control datepicker',
                'placeholder' => ''
            ),set_value('date_issued',$issued->date_issued)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="quantity_issued" class="col-sm-2 control-label">Quantity</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'quantity_issued',
                'type' => 'text',
                'name' => 'quantity_issued',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('quantity_issued',$issued->quantity_issued)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="expiry_date" class="col-sm-2 control-label">Expiry Date</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'expiry_date',
                'type' => 'text',
                'name' => 'expiry_date',
                'class' => 'form-control datepicker',
                'placeholder' => ''
            ),set_value('expiry_date',$issued->expiry_date)) ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
    <?php echo form_close() ?>
</div>
<script>
    $(document).ready(
        function(){
            $('.datepicker').datepicker({
                format : 'yyyy-mm-dd'
            })
        }
    )
</script>