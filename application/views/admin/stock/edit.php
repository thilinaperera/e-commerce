<div class="modal-header">
    <h2 class="modal-title text-left"><?php echo empty($stock->id) ? 'Add a new stock' : 'Edit stock' ?></h2>
</div>
<div class="modal-body">
    <?php echo validation_errors() ?>

    <?php echo form_open('',array( 'class' => 'form-horizontal','role' => 'form' )) ?>
    <div class="form-group">
        <label for="supplier_id" class="col-sm-2 control-label">Product</label>
        <div class="col-sm-10">
            <?php echo form_dropdown('product_id',$products,$this->input->post('product_id') ? $this->input->post('product_id') : $stock->product_id,'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'description',
                'type' => 'text',
                'name' => 'description',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('description',$stock->description)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="in_date" class="col-sm-2 control-label">Stock in date</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'in_date',
                'type' => 'text',
                'name' => 'in_date',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('in_date',$stock->in_date)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="in_quantity" class="col-sm-2 control-label">Quantity</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'in_quantity',
                'type' => 'text',
                'name' => 'in_quantity',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('in_quantity',$stock->in_quantity)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="unit_price" class="col-sm-2 control-label">Unit price</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'unit_price',
                'type' => 'text',
                'name' => 'unit_price',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('unit_price',$stock->unit_price)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="supplier_id" class="col-sm-2 control-label">Supplier</label>
        <div class="col-sm-10">
            <?php echo form_dropdown('supplier_id',$suppliers,$this->input->post('supplier_id') ? $this->input->post('supplier_id') : $stock->supplier_id,'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
    <?php echo form_close() ?>
</div>
<script>
    $(document).ready(
        function(){
            $('#in_date').datepicker({
                format : 'yyyy-mm-dd'
            })
        }
    )
</script>