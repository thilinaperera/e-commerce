<section>
    <h2>Stocks</h2>
    <?php echo anchor('admin/stock/edit','<i class="glyphicon glyphicon-plus"></i> Add stock'); ?>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Product</th>
            <th>Description</th>
            <th>Stock in date</th>
            <th>Quantity</th>
            <th>Unit price</th>
            <th>Supplier</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($stocks);
            if(count($stocks)){
                foreach($stocks as $stock){ ?>
                    <tr>
                        <td><?php echo anchor('admin/stock/edit/'.$stock->id, $stock->id) ?></td>
                        <td><?php echo $stock->product_id ?></td>
                        <td><?php echo $stock->description ?></td>
                        <td><?php echo $stock->in_date ?></td>
                        <td><?php echo $stock->in_quantity ?></td>
                        <td><?php echo $stock->unit_price ?></td>
                        <td><?php echo $stock->supplier_id ?></td>
                        <td><?php echo btn_edit('admin/stock/edit/'.$stock->id) ?></td>
                        <td><?php echo btn_delete('admin/stock/delete/'.$stock->id) ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>