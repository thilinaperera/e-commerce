<section>
    <h2>Suppliers</h2>
    <?php echo anchor('admin/supplier/edit','<i class="glyphicon glyphicon-plus"></i> Add supplier'); ?>
        <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            <?php
            //var_dump($suppliers);
            if(count($suppliers)){
                foreach($suppliers as $supplier){ ?>
                    <tr>
                        <td><?php echo anchor('admin/supplier/edit/'.$supplier->id, $supplier->name) ?></td>
                        <td><?php echo $supplier->email ?></td>
                        <td><?php echo $supplier->phone ?></td>
                        <td><?php echo $supplier->description ?></td>
                        <td><?php echo btn_edit('admin/supplier/edit/'.$supplier->id) ?></td>
                        <td><?php echo btn_delete('admin/supplier/delete/'.$supplier->id) ?></td>
                    </tr>
            <?php
                }
            }
            else{ ?>
                <tr>
                    <td>No records found</td>
                </tr>
            <?php
            }
            ?>




        </tbody>
    </table>
</section>