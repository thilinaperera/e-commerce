<div class="modal-header">
    <h2 class="modal-title text-left"><?php echo empty($supplier->id) ? 'Add a new supplier' : 'Edit supplier' ?></h2>
</div>
<div class="modal-body">
    <?php echo validation_errors() ?>
    <?php echo form_open('',array( 'class' => 'form-horizontal','role' => 'form' )) ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'name',
                'type' => 'text',
                'name' => 'name',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('name',$supplier->name)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'email',
                'type' => 'email',
                'name' => 'email',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('email',$supplier->email)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Phone</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'phone',
                'type' => 'text',
                'name' => 'phone',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('phone',$supplier->phone)) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'description',
                'type' => 'text',
                'name' => 'description',
                'class' => 'form-control',
                'placeholder' => ''
            ),set_value('description',$supplier->description)) ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
    <?php echo form_close() ?>
</div>