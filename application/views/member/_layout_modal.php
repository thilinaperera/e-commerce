<?php $this->load->view('admin/components/page_head.php') ?>
<body style="background: #eeeeee">

<div class="modal show">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $this->load->view($subview) ?>
            <div class="modal-footer text-right">
                <p>&copy; <?php echo $meta_title ?></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php $this->load->view('admin/components/page_footer.php') ?>