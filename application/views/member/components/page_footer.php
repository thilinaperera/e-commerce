<footer class="navbar-inverse">
    <div class="container  ">
        <p class="text-right">&copy; Ananda Foodcity (Pvt) Ltd</p>
    </div>
</footer>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('js/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('js/isotope.pkgd.min.js'); ?>"></script>
<script>
    $("a").each(function () {
        //console.log($(this).attr("href"))

        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
        }

        if ($(this).attr("href") == window.location.href) {
            $(this).parent().addClass("active");
        }

    });
    $( function() {
        // init Isotope
        var $container = $('.productHolder').isotope({
            itemSelector: '.productItem',
            layoutMode: 'masonry'
        });
    })

</script>
</body>
</html>