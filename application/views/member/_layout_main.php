<?php $this->load->view('member/components/page_head.php') ?>
<body>
<nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url('member/'); ?>"><?php echo $meta_title ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo site_url('member/'); ?>">Home</a></li>
                <li><?php echo anchor('member/edit','Profile'); ?></li>
                <li><?php echo anchor('shopping/cart','Cart'); ?></li>
                <li><?php echo anchor('shopping/order','My Orders'); ?></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><?php echo anchor('','<i class="glyphicon glyphicon-user"></i> '.$this->session->userdata("name").'') ?></li>
                <li> <?php echo anchor('member/logout','<i class="glyphicon glyphicon-log-out"></i> logout') ?></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container marginAdded">
    <?php if($this->session->flashdata('error')){ ?>
        <div class="alert alert-danger fade in">
            <p><?php echo $this->session->flashdata('error'); ?></p>
        </div>
    <?php } ?>
    <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success fade in">
            <p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <?php $this->load->view($subview)?>
        </div>
        <div class="col-xs-4 col-md-4">
            <?php $this->load->view($sidebar) ?>
        </div>
    </div>
</div>
<?php $this->load->view('member/components/page_footer.php') ?>
<script>
    $(document).ready(function(){
        $(".alert").alert();
        setTimeout(function(){
            $(".alert").alert('close');
        }, 3000);

    })
</script>
