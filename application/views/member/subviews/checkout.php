<?php //echo json_encode($this->cart->contents());  ?>
<div class="row">
<h2>Please add Details</h2>
    <div class="col-lg-12">
    <?php echo validation_errors() ?>
    <?php echo form_open('shopping/checkout')  ?>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    STEP 1: ACCOUNT &amp; BILLING DETAILS
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse in" style="height: 0px;">
            <div class="panel-body">
                <div class="row">
                    <div class="col-12 col-lg-12">
                        <!-- FORM -->


                                <legend>
                                    Enter your billing details
                                </legend>

                                <div class="form-group">
                                    <label for="FirstName">
                                        First Name
                                    </label>
                                    <?php echo form_input(array(
                                        'id' => 'billing_name_first',
                                        'type' => 'text',
                                        'name' => 'billing_name_first',
                                        'class' => 'form-control',
                                        'placeholder' => ''
                                    ),set_value('billing_name_first',$order->billing_name_first)) ?>
                                </div>
                                <div class="form-group">
                                    <label for="LastName">
                                        Last Name
                                    </label>
                                    <?php echo form_input(array(
                                        'id' => 'billing_name_last',
                                        'type' => 'text',
                                        'name' => 'billing_name_last',
                                        'class' => 'form-control',
                                        'placeholder' => ''
                                    ),set_value('billing_name_last',$order->billing_name_last)) ?>
                                </div>
                                <div class="form-group">
                                    <label for="Adress1">
                                        Adress 1
                                    </label>
                                    <?php echo form_input(array(
                                        'id' => 'billing_address1',
                                        'type' => 'text',
                                        'name' => 'billing_address1',
                                        'class' => 'form-control',
                                        'placeholder' => ''
                                    ),set_value('billing_address1',$order->billing_address1)) ?>
                                </div>
                                <div class="form-group">
                                    <label for="Adress2">
                                        Adress 2
                                    </label>
                                    <?php echo form_input(array(
                                        'id' => 'billing_address2',
                                        'type' => 'text',
                                        'name' => 'billing_address2',
                                        'class' => 'form-control',
                                        'placeholder' => ''
                                    ),set_value('billing_address2',$order->billing_address2)) ?>
                                </div>


                        <!-- /FORM -->
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        STEP 2: SHIPPING DETAILS
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-12 col-lg-12">

                                    <legend>
                                        Enter your shipping details
                                    </legend>
                                    <div class="form-group">
                                        <label for="FirstName">
                                            First Name
                                        </label>
                                        <?php echo form_input(array(
                                            'id' => 'shipping_name_first',
                                            'type' => 'text',
                                            'name' => 'shipping_name_first',
                                            'class' => 'form-control',
                                            'placeholder' => ''
                                        ),set_value('shipping_name_first',$order->shipping_name_first)) ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="LastName">
                                            Last Name
                                        </label>
                                        <?php echo form_input(array(
                                            'id' => 'shipping_name_last',
                                            'type' => 'text',
                                            'name' => 'shipping_name_last',
                                            'class' => 'form-control',
                                            'placeholder' => ''
                                        ),set_value('shipping_name_last',$order->shipping_name_last)) ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="Adress1">
                                            Adress 1
                                        </label>
                                        <?php echo form_input(array(
                                            'id' => 'shipping_address1',
                                            'type' => 'text',
                                            'name' => 'shipping_address1',
                                            'class' => 'form-control',
                                            'placeholder' => ''
                                        ),set_value('shipping_address1',$order->shipping_address1)) ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="Adress2">
                                            Adress 2
                                        </label>
                                        <?php echo form_input(array(
                                            'id' => 'shipping_address2',
                                            'type' => 'text',
                                            'name' => 'shipping_address2',
                                            'class' => 'form-control',
                                            'placeholder' => ''
                                        ),set_value('shipping_address2',$order->shipping_address2)) ?>
                                    </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                    STEP 5: PAYMENT METHOD
                </a>
            </h4>
        </div>
        <div id="collapseFive" class="panel-collapse collapse" style="height: 0px;">
            <div class="panel-body">
                <div class="row">
                    <div class="col-12 col-lg-12">

                                <legend>CHOOSE PAYMENT METHOD</legend>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                                        PayPal
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Cash on Delivery
                                    </label>
                                </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                    STEP 6: CONFIRM ORDER
                </a>
            </h4>
        </div>
        <div id="collapseSix" class="panel-collapse collapse" style="height: 0px;">
            <div class="panel-body">
                <div class="row">
                    <div class="col-12 col-lg-12">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            if( $this->cart->contents() ){
                                foreach($this->cart->contents() as $item){ ?>
                                    <tr>
                                        <td><?php  echo $item['name'] ?></td>
                                        <td><?php  echo $item['qty'] ?></td>
                                        <td><?php  echo $item['price'] ?></td>
                                        <td><?php  echo $item['subtotal'] ?></td>
                                    </tr>

                                <?php    }
                            }
                            else{
                                echo '<tr>Cart is empty</tr>';
                            }

                            ?>


                            </tbody>
                        </table>
                        <hr>
                        <dl class="dl-horizontal pull-right">
                            <dt>Total:</dt>
                            <dd>Rs <?php  echo $this->cart->total() ?></dd>
                        </dl>
                        <div class="clearfix"></div>

                    </div>
                </div>

            </div>

        </div>

    </div>

    </div>
    <button type="submit" class="btn btn-info pull-right">Send to finalize</button>
    <?php echo form_close()  ?>


    </div>
</div>
