<div class="list-group">
        <a class="list-group-item"  href="#" style="width:100%;">
            <i class=" glyphicon glyphicon-shopping-cart"></i>  Your Bag <span class="badge pull-right">Rs <?php echo $this->cart->total() ?></span>
        </a>
        <a href="#" class="list-group-item active text-center">
            Cart Details
        </a>
        <?php
        if( $this->cart->contents() ){
            foreach($this->cart->contents() as $item){
                ?>

                <a href="#" class="list-group-item">
                    <span class="badge badge-danger"><?php  echo $item['subtotal'] ?></span>
                    <?php echo $item['name'] ?>
                    <span class="badge "><?php  echo $item['qty'] ?> * <?php  echo $item['price'] ?>  = </span>

                </a>
               <?php
            }
            ?>
            <span  class="list-group-item">
                        <a href="<?php echo base_url('shopping/cart'); ?>" class="btn btn-info navbar-btn" style=" color:#fff; width: 100%">Update Cart</a>
            </span>
            <span  class="list-group-item">
                        <a href="<?php echo base_url('shopping/checkout'); ?>" class="btn btn-success navbar-btn" style=" color:#fff; width: 100%">Proceed to checkout</a>
            </span>
        <?php
        }else{
            ?>
            <a href="#" class="list-group-item text-center">

                Empty Cart

            </a>
            <?php
        }
        ?>
</div>