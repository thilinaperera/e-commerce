<?php //echo json_encode($this->cart->contents());  ?>
<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Shoping Cart</h3>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        if( $this->cart->contents() ){
                            foreach($this->cart->contents() as $item){ ?>
                                <tr>
                                    <td><?php  echo $item['name'] ?></td>
                                    <td><?php  echo $item['qty'] ?></td>
                                    <td><?php  echo $item['price'] ?></td>
                                    <td><?php  echo $item['subtotal'] ?></td>
                                    <td><?php echo btn_delete('shopping/remove/'.$item['rowid']) ?></td>
                                </tr>

                        <?php    }
                        }
                        else{
                            echo '<tr>Cart is empty</tr>';
                        }

                        ?>


                        </tbody>
                    </table>
                    <hr>
                    <dl class="dl-horizontal pull-right">
                        <dt>Total:</dt>
                        <dd>Rs <?php echo $this->cart->total() ?></dd>
                    </dl>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo anchor('shopping/checkout','<i class="glyphicon glyphicon-ok"></i> Proceed to checkout',array('class' => 'btn btn-success pull-right')); ?>
                </div>
            </div>
        </div>
    </div>

</div>
