<?php //echo json_encode($this->cart->contents());  ?>
<div class="row productHolder">

    <?php
    if(count($products)){
        //print_r($products);
        foreach($products as $product){
            ?>
            <?php echo form_open('shopping/add') ?>
            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 productItem">
                <div class="thumbnail">
                    <!-- IMAGE CONTAINER-->
                    <img src="<?php echo site_url('uploads/'.$product->image); ?>" alt="...">
                    <!--END IMAGE CONTAINER-->
                    <!-- CAPTION -->
                    <div class="caption">
                        <h3 class=""><?php  echo $product->name; ?></h3>
                        <p class=""><?php   echo $product->description; ?></p>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p class=""><b>Price </b><span class="label label-warning">Rs <?php echo $product->price; ?></span></p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p class="margintopadded"><b>Quantity</b></p>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <input name="qty" type="text" class="form-control" placeholder="" value="1">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center addtoHolder">
                                <?php
                                $data = array(
                                    'name' => 'button',
                                    'id' => '',
                                    'class' => 'btn btn-success btn-block',
                                    'value' => '',
                                    'type' => 'submit',
                                    'content' => 'Add to cart'
                                );

                                echo form_button($data);
                                ?>
                            </div>
                        </div>
                    </div>
                    <!--END CAPTION -->
                </div>
                <!-- END: THUMBNAIL -->
                <?php  echo form_hidden('id',$product->id)  ?>
            </div>

            <?php echo form_close() ?>
        <?php
        }
    }
    ?>


</div>
