<div class="modal-header">
    <h2 class="modal-title text-left">Register </h2>
</div>
<div class="modal-body">

    <?php echo validation_errors() ?>
    <?php echo form_open('',array( 'class' => 'form-horizontal','role' => 'form' )) ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'name',
                'type' => 'text',
                'name' => 'name',
                'class' => 'form-control',
                'placeholder' => ''
            )) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'email',
                'type' => 'email',
                'name' => 'email',
                'class' => 'form-control',
                'placeholder' => ''
            )) ?>
        </div>
    </div>

    <div class="form-group">
        <label for="password" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'password',
                'type' => 'password',
                'name' => 'password',
                'class' => 'form-control',
                'placeholder' => ''
            )) ?>
        </div>
    </div>
    <div class="form-group">
        <label for="password_confirm" class="col-sm-2 control-label">Confirm Password</label>
        <div class="col-sm-10">
            <?php echo form_input(array(
                'id' => 'password_confirm',
                'type' => 'password',
                'name' => 'password_confirm',
                'class' => 'form-control',
                'placeholder' => ''
            )) ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
        </div>
    </div>
    <?php echo form_close() ?>
</div>