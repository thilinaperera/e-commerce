<section>
    <h2>Orders</h2>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Products in cart</th>
            <th>Date orders</th>
            <th>Shipping Address</th>
            <th>Billing Address</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <?php
        //var_dump($orderss);
        if(count($orders)){
            foreach($orders as $order){
                //var_dump($order);
                //var_dump($this->shopping_m->get_all_orders_with_cart())
                ?>

                <tr>
                    <td><?php echo $order->id ?></td>
                    <td>
                        <?php
                        $carts = $this->shopping_m->get_all_order_cart($order->id);
                        $total = null;
                        foreach($carts as $cart){
                            ?>
                            <li style="font-size: 12px;"><?php echo $cart['name'] ?></li>
                        <?php

                        }
                        ?>
                    </td>

                    <td><?php echo $order->date ?></td>
                    <td><?php echo $order->shipping_address1 ?><br/><?php echo $order->shipping_address2 ?></td>
                    <td><?php echo $order->billing_address1 ?><br/><?php echo $order->billing_address2 ?></td>
                    <td><?php
                        if($order->status == 'complete'){
                            echo '<span class="label label-success">Complete</span>';
                        }
                        elseif($order->status == 'rejected'){
                            echo '<span class="label label-danger">Rejected</span>';
                        }
                        elseif($order->status == 'pending'){
                            echo '<span class="label label-info">Pending</span>';
                        }
                        ?>
                    </td>
                </tr>
            <?php
            }
        }
        else{ ?>
            <tr>
                <td>No records found</td>
            </tr>
        <?php
        }
        ?>




        </tbody>
    </table>
</section>