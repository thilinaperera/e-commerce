<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */

class Member extends Member_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('member_m');
        $this->load->model('product_m');


    }

    public function index(){


        $this->data['products'] = $this->product_m->get_with_price();

        $this->data['subview'] = 'member/subviews/index';
        $this->data['sidebar'] = 'member/subviews/sidebar';
        $this->load->view('member/_layout_main',$this->data);


    }
    public function edit(){

        // update himself
        //$this->session->userdata("email");
        $id = $this->session->userdata("id");
        if($id){
            $this->data['user'] = $this->member_m->get($id);
            if(!count($this->data['user'])){
                $this->data['errors'][] = "User could not be found";
                //var_dump($this->data);
            }
        }
        else{
            // not a logged user
            // immediate redirect to login
            $this->data['subview'] = 'member/subviews/login';
            $this->data['sidebar'] = 'member/subviews/sidebar';
            $this->load->view('member/_layout_main',$this->data);

        }
        $rules = $this->member_m->rules_user ;
        if($id != null){
            $rules['password']['rules'] .= '|required';
        }


        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            // ok please save this data
            $data = $this->member_m->array_from_post(array('name','email','password','usertype_id'));
            $data['password'] = $this->member_m->hash($data['password']);

            // set user type to normal user
            $data['usertype_id'] = 2;
            $this->member_m->save($data, $id);
            redirect('member');

        }
        $this->data['subview'] = 'member/subviews/edit';
        $this->data['sidebar'] = 'member/subviews/sidebar';
        $this->load->view('member/_layout_main',$this->data);

    }
    public function register(){
        // register user

        $rules = $this->member_m->rules_admin ;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            // ok please save this data
            $data = $this->member_m->array_from_post(array('name','email','password','usertype_id'));
            $data['password'] = $this->member_m->hash($data['password']);

            // set user type to normal user
            $data['usertype_id'] = '2';
            $this->member_m->save($data, null);
            redirect('member/');

        }
        $this->data['subview'] = 'member/subviews/register';
        $this->load->view('member/_layout_modal',$this->data);

    }
    public function delete($id){

        // deactivate account
        $this->member_m->delete($id);
        redirect('user/subviews/login');
        $this->load->view('user/_layout_main',$this->data);

    }
    public function login(){

        $memberDashboard = 'member';
        $dashboard = 'admin/dashboard';

        if($this->member_m->loggedin()){
            // send logged user to dashboard
            //redirect($userDashboard);
            if($this->member_m->is_member()){
                redirect($memberDashboard);
            }else{
                redirect($dashboard);
            }

        }


        $rules = $this->member_m->rules ;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            //var_dump($this->member_m->login());
            if($this->member_m->login() == true ){
                // user successfully logged in
                redirect($memberDashboard);
                    if($this->member_m->is_member()==true){
                        redirect($memberDashboard);
                    }
                    else{
                        redirect($dashboard);
                    }
            }
            else{
                $this->session->set_flashdata('error','That email/password combination does not match or not exist');
                redirect('admin/user/login');
            }
        }

        $this->data['subview'] = 'member/subviews/login';
        $this->load->view('member/_layout_modal',$this->data);
        //var_dump($this->session->userdata);

    }
    public function logout(){
        $this->member_m->logout();
        redirect('admin/user/login');
    }
    public function _unique_email($str){

        $id = $this->uri->segment(4);
        $this->db->where('email',$this->input->post('email'));
        !$id || $this->db->where('id !=', $id);
        $user = $this->member_m->get();

        if(count($user)){
            $this->form_validation->set_message('_unique_email','%s should be unique');
            return false;
        }
        return true;
    }
}


