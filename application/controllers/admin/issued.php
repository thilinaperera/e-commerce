<?php
/**
 * Created by PhpStorm.
 * issued: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */
class Issued extends Admin_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('issued_m');
    }

    public function index(){
        $this->data['issueds'] = $this->issued_m->get();
        $this->data['subview'] = 'admin/issued/index';
        $this->load->view('admin/_layout_main',$this->data);
        //var_dump($this->data['issueds']);
    }
    public function edit($id = null){
        if($id){
            $this->data['issued'] = $this->issued_m->get($id);
            if(!count($this->data['issued'])){
                $this->data['errors'][] = "issued could not be found";
            }
        }
        else{
            // this will return empty issued details
            $this->data['issued'] = $this->issued_m->get_new();

        }

        $rules = $this->issued_m->rules ;
        // products for dropdown
        $this->data['stocks'] = $this->issued_m->get_with_stocks();
        $this->data['products'] = $this->issued_m->get_with_products();
        //var_dump($this->data['status']);
        $this->form_validation->set_rules($rules);

        if($this->form_validation->run()==true){


            // ok please save this data
            $data = $this->issued_m->array_from_post(array('stock_id','product_id','date_issued','quantity_issued','expiry_date'));

            // check remaining quantity
            $data['product_id'] = $this->issued_m->get_product_id($data['stock_id']);
            $inreturn = $this->issued_m->get_return_amount($data['stock_id']);
            $inissued = $this->issued_m->get_old_issued_amount($data['stock_id'],$id); // without current issued amount
            $total = $inreturn+$inissued+ (float)$data['quantity_issued'];
            $stock_in_quantity =  $this->issued_m->get_stock_quantity($data['stock_id']);
            $maximum_amount = $stock_in_quantity-$total;
            if($maximum_amount<0){
                //echo 'Please enter valid quantity';
                $this->session->set_flashdata('error', 'Sorry please enter valid quantity');
                redirect('admin/issued/edit/'.$id);
            }
            else{
                $this->issued_m->save($data, $id);
                redirect('admin/issued');
            }

        }
        $this->data['subview'] = 'admin/issued/edit';
        $this->load->view('admin/_layout_main',$this->data);


    }
    public function delete($id){
        $this->issued_m->delete($id);
        redirect('admin/issued');
    }
}


