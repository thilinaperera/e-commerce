<?php
/**
 * Created by PhpStorm.
 * returning: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */
class Returning extends Admin_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('returning_m');
    }

    public function index(){
        $this->data['returns'] = $this->returning_m->get_with_product_names();
        $this->data['subview'] = 'admin/returning/index';
        $this->load->view('admin/_layout_main',$this->data);
    }
    public function issued($id = null){

        if($id){
            $this->data['returning'] = $this->returning_m->get_without_stock_returns($id);
            if(!count($this->data['returning'])){
                $this->data['errors'][] = "returning could not be found";
            }
        }
        else{
            // this will returning empty returning details
            $this->data['returning'] = $this->returning_m->get_new();

        }
        // var_dump($this->data['returning']);
        $rules = $this->returning_m->rules_issued ;
        // products for dropdown
        $this->data['issues'] = $this->returning_m->get_with_issues();

        $this->form_validation->set_rules($rules);

        if($this->form_validation->run()==true){

            // ok please save this data
            $data = $this->returning_m->array_from_post(array('issued_id','product_id','date','quantity','comment'));
            // get product id from issued table
            $data['product_id'] = $this->returning_m->get_product_form_issued($data['issued_id']);
            // check remaining quantity
            // get total quantity from returns where current issued
            $total_returns_form_issued = $this->returning_m->total_returns_form_issued($data['issued_id']);
            $current_issued_quantity = $this->returning_m->get_current_issued_quantity($data['issued_id']);
            $current_retuning_quantity = $data['quantity'];
            $maximum_quantity = $current_issued_quantity - ($total_returns_form_issued+$current_retuning_quantity);

            /*
            var_dump($total_returns_form_issued."--total_returns_form_issued");
            var_dump($current_issued_quantity."--current_issued_quantity");
            var_dump($maximum_quantity);

            //$this->returning_m->save($data, $id);
            //redirect('admin/returning');
            */
            if($maximum_quantity < 0){
                //echo 'Please enter valid quantity';
                $this->session->set_flashdata('error', 'Sorry please enter valid quantity');
                redirect('admin/returning/issued/'.$id);
            }
            else{
                //echo 'ok';
                $this->session->set_flashdata('success', 'Record updated');
                $this->returning_m->save($data, $id);
                redirect('admin/returning');
            }

        }
        $this->data['subview'] = 'admin/returning/issued/edit';
        $this->load->view('admin/_layout_main',$this->data);

    }
    public function stock($id = null){
        if($id){
            $this->data['returning'] = $this->returning_m->get($id);
            if(!count($this->data['returning'])){
                $this->data['errors'][] = "returning could not be found";
            }
        }
        else{
            // this will returning empty returning details
            $this->data['returning'] = $this->returning_m->get_new();

        }
       // var_dump($this->data['returning']);
        $rules = $this->returning_m->rules_stock ;
        // products for dropdown
        $this->data['stocks'] = $this->returning_m->get_with_stocks();
        $this->data['products'] = $this->returning_m->get_with_products();
        //var_dump($this->data['status']);
        $this->form_validation->set_rules($rules);

        if($this->form_validation->run()==true){


            // ok please save this data
            $data = $this->returning_m->array_from_post(array('stock_id','product_id','date','quantity','comment'));

            $data['product_id'] = $this->returning_m->get_product_id($data['stock_id']);

            // check remaining quantity
            $inreturn= $this->returning_m->get_return_amount($data['stock_id']);
            $inissued = $this->returning_m->get_old_issued_amount($data['stock_id'],$id); // without current returning amount
            $total = $inreturn+$inissued+ (float)$data['quantity'];
            $stock_in_quantity =  $this->returning_m->get_stock_quantity($data['stock_id']);
            $maximum_amount = $stock_in_quantity-$total;

            /*var_dump($inreturn."--inreturn");
            var_dump($inissued."--inissued");
            var_dump($total);
            var_dump($stock_in_quantity-$total);
            //$this->returning_m->save($data, $id);
            //redirect('admin/returning');*/

            if($maximum_amount<0){
                echo 'Please enter valid quantity';
                $this->session->set_flashdata('error', 'Sorry please enter valid quantity');
                redirect('admin/returning/stock/'.$id);
            }
            else{
                $this->returning_m->save($data, $id);
                $this->session->set_flashdata('success', 'Record updated');
                redirect('admin/returning');
            }

        }
        $this->data['subview'] = 'admin/returning/stock/edit';
        $this->load->view('admin/_layout_main',$this->data);

    }
    public function delete($id){
        $this->returning_m->delete($id);
        redirect('admin/returning');
    }
}


