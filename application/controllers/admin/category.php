<?php
/**
 * Created by PhpStorm.
 * category: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */

class Category extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('category_m');
    }

    public function index(){
        $this->data['categories'] = $this->category_m->get();
        $this->data['subview'] = 'admin/category/index';
        $this->load->view('admin/_layout_main',$this->data);
        //var_dump($this->data['categories']);
    }
    public function edit($id = null){
        if($id){
            $this->data['category'] = $this->category_m->get($id);
            if(!count($this->data['category'])){
                $this->data['errors'][] = "category could not be found";
            }
        }
        else{
            // this will return empty category details
            $this->data['category'] = $this->category_m->get_new();
        }
       // var_dump($this->data['category']);
        $rules = $this->category_m->rules ;


        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            // ok please save this data
            $data = $this->category_m->array_from_post(array('name','description'));
            $this->category_m->save($data, $id);
            redirect('admin/category');

        }
        $this->data['subview'] = 'admin/category/edit';
        $this->load->view('admin/_layout_main',$this->data);



    }
    public function delete($id){
        $this->category_m->delete($id);
        redirect('admin/category');
    }
}


