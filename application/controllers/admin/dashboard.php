<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 12:01 PM
 */
class Dashboard extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('dashboard_m');
        $this->load->model('user_m');
        $this->load->model('product_m');


    }

    public function index(){
        $this->data['users'] = $this->user_m->get_users_count_all();
        $this->data['products'] = $this->product_m->get();

        $this->data['subview'] = 'admin/dashboard/index';
        $this->data['issued_chart'] = $this->dashboard_m->get_product_issued();
        // custom
        $this->data['issued_chart'] = $this->dashboard_m->get_product_issued();
        $this->data['returns'] = $this->dashboard_m->get_returns();
        $this->load->view('admin/_layout_dashboard',$this->data);
        //var_dump($this->data['returns']);

    }
    public function modal(){
        $this->load->view('admin/_layout_modal',$this->data);
    }
}