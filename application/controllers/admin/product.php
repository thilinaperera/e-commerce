<?php
/**
 * Created by PhpStorm.
 * product: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */

class Product extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('product_m');
    }

    public function index(){
        $this->data['products'] = $this->product_m->get_cat_and_brand();
        $this->data['subview'] = 'admin/product/index';
        $this->load->view('admin/_layout_main',$this->data);
        //var_dump($this->data['products']);
    }
    public function edit($id = null){
        // for random strings
        $this->load->helper('string');
        //  random_string('alnum', 10);


        // config uploads

        $config['upload_path']   =   "./uploads/";
        $config['allowed_types'] =   "gif|jpg|jpeg|png";
        $config['file_name'] =   random_string('alnum', 10);
        $config['max_size']      =   "5000";
        $config['max_width']     =   "1907";
        $config['max_height']    =   "1280";

        // load upload helper
        $this->load->library('upload',$config);


        // end
        if($id){
            $this->data['product'] = $this->product_m->get($id);
            if(!count($this->data['product'])){
                $this->data['errors'][] = "product could not be found";
            }
        }
        else{
            // this will return empty product details
            $this->data['product'] = $this->product_m->get_new();
        }

        $rules = $this->product_m->rules ;

        // products for dropdown
        $this->data['categories'] = $this->product_m->get_with_categories();
        $this->data['brands'] = $this->product_m->get_with_brands();

        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            // ok please save this data
            $saveData = $this->product_m->array_from_post(array('name','description','category_id','brand_id','image'));
            $imageData = $this->product_m->array_from_post(array('image_check'));
            var_dump($imageData);
            $data = array();

            // image upload function
            if(!$this->upload->do_upload('image'))
            {
                echo $this->upload->display_errors();
            }
            else{
                $finfo = $this->upload->data();
                var_dump($finfo);
                $this->_createThumbnail($finfo['file_name']);
                $data['uploadInfo'] = $finfo;
                $data['thumbnail_name'] = $finfo['raw_name']. '_thumb' .$finfo['file_ext'];
                //echo 'done';
            }
            if(count($data)){
                //var_dump($data['uploadInfo']['orig_name']);
                $saveData['image'] = $data['uploadInfo']['orig_name'];
            }
            else{
                $saveData['image'] = $imageData['image_check'];
            }

            $this->product_m->save($saveData, $id);
            redirect('admin/product');

        }




        $this->data['subview'] = 'admin/product/edit';
        $this->load->view('admin/_layout_main',$this->data);
        //var_dump($this->data);


    }
    public function delete($id){
        $this->product_m->delete($id);
        redirect('admin/product');
    }
    //Create Thumbnail function
    function _createThumbnail($filename)

    {

        $config['image_library']    = "gd2";

        $config['source_image']     = "uploads/" .$filename;

        $config['create_thumb']     = TRUE;

        $config['maintain_ratio']   = TRUE;

        $config['width'] = "80";

        $config['height'] = "80";

        $this->load->library('image_lib',$config);

        if(!$this->image_lib->resize())

        {

            echo $this->image_lib->display_errors();

        }

    }

}


