<?php
/**
 * Created by PhpStorm.
 * brand: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */

class Report extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('brand_m');
        $this->load->model('user_m');
        $this->load->model('supplier_m');
        $this->load->model('returning_m');
        $this->load->model('issued_m');
    }

    public function edit($id = null){
        if($id){
            $this->data['brand'] = $this->brand_m->get($id);
            if(!count($this->data['brand'])){
                $this->data['errors'][] = "brand could not be found";
            }
        }
        else{
            // this will return empty brand details
            $this->data['brand'] = $this->brand_m->get_new();
        }
       // var_dump($this->data['brand']);
        $rules = $this->brand_m->rules ;


        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            // ok please save this data
            $data = $this->brand_m->array_from_post(array('name','description'));
            $this->brand_m->save($data, $id);
            redirect('admin/brand');

        }
        $this->data['subview'] = 'admin/brand/edit';
        $this->load->view('admin/_layout_main',$this->data);


    }
    public function delete($id){
        $this->brand_m->delete($id);
        redirect('admin/brand');
    }
    public function users(){
        $this->data['users'] = $this->user_m->get_with_type();
       // var_dump($users);
        $this->data['subview'] = 'admin/report/user';
        $this->load->view('admin/_layout_report',$this->data);
    }
    public function suppliers(){
        $this->data['suppliers'] = $this->supplier_m->get();
        // var_dump($users);
        $this->data['subview'] = 'admin/report/supplier';
        $this->load->view('admin/_layout_report',$this->data);
    }
    public function returns(){
        $this->data['returns'] = $this->returning_m->get_with_product_names();
       // var_dump($users);
        $this->data['subview'] = 'admin/report/returns';
        $this->load->view('admin/_layout_report',$this->data);
    }

    public function orders(){
        $this->data['orders'] = $this->shopping_m->get_all_orders();
        $this->data['subview'] = 'admin/report/orders';
        $this->load->view('admin/_layout_report',$this->data);
        //var_dump($this->data['orders']);
    }

    public function issued(){
        $this->data['issues'] = $this->issued_m->get_with_product_names();
        //var_dump( $this->data['issues']);
        $this->data['subview'] = 'admin/report/issued';
        $this->load->view('admin/_layout_report',$this->data);
        //var_dump($this->data['orders']);
    }
}


