<?php
/**
 * Created by PhpStorm.
 * page: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */

class Page extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('page_m');
    }

    public function index(){
        $this->data['pages'] = $this->page_m->get_with_parent();
        $this->data['subview'] = 'admin/page/index';
        $this->load->view('admin/_layout_main',$this->data);
        //var_dump($this->data['pages']);
    }
    public function edit($id = null){
        if($id){
            $this->data['page'] = $this->page_m->get($id);
            if(!count($this->data['page'])){
                $this->data['errors'][] = "page could not be found";
            }
        }
        else{
            // this will return empty page details
            $this->data['page'] = $this->page_m->get_new();
        }

        $rules = $this->page_m->rules ;

        // pages for dropdown
        $this->data['pages_no_parents'] = $this->page_m->get_no_parents();


        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            // ok please save this data
            $data = $this->page_m->array_from_post(array('title','slug','order','body','parent_id'));
            $this->page_m->save($data, $id);
            redirect('admin/page');

        }
        $this->data['subview'] = 'admin/page/edit';
        $this->load->view('admin/_layout_main',$this->data);

    }
    public function delete($id){
        $this->page_m->delete($id);
        redirect('admin/page');
    }
    public function login(){

        $dashboard = 'admin/dashboard';
        if($this->page_m->loggedin()){
            // send logged page to dashboard
            redirect($dashboard);
        }


        $rules = $this->page_m->rules ;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            if($this->page_m->login() == true ){
                // page successfully logged in
                redirect($dashboard);
            }
            else{
                $this->session->set_flashdata('error','That email/password combination does not match or not exist');
                redirect('admin/page/login');
            }
        }
        $this->data['subview'] = 'admin/page/login';
        $this->load->view('admin/_layout_modal',$this->data);
    }
    public function logout(){
        $this->page_m->logout();
        redirect('admin/page/login');
    }
    public function _unique_slug($str){

        $id = $this->uri->segment(4);
        $this->db->where('slug',$this->input->post('slug'));
        !$id || $this->db->where('id !=', $id);
        $page = $this->page_m->get();

        if(count($page)){
            $this->form_validation->set_message('_unique_slug','%s should be unique');
            return false;
        }
        return true;
    }

}


