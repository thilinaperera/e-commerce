<?php
/**
 * Created by PhpStorm.
 * supplier: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */

class Supplier extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('supplier_m');
    }

    public function index(){
        $this->data['suppliers'] = $this->supplier_m->get();
        $this->data['subview'] = 'admin/supplier/index';
        $this->load->view('admin/_layout_main',$this->data);
        //var_dump($this->data['suppliers']);
    }
    public function edit($id = null){
        if($id){
            $this->data['supplier'] = $this->supplier_m->get($id);
            if(!count($this->data['supplier'])){
                $this->data['errors'][] = "supplier could not be found";
            }
        }
        else{
            // this will return empty supplier details
            $this->data['supplier'] = $this->supplier_m->get_new();
        }
       // var_dump($this->data['supplier']);
        $rules = $this->supplier_m->rules ;


        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            // ok please save this data
            $data = $this->supplier_m->array_from_post(array('name','email','phone','description'));
            $this->supplier_m->save($data, $id);
            redirect('admin/supplier');

        }
        $this->data['subview'] = 'admin/supplier/edit';
        $this->load->view('admin/_layout_main',$this->data);


    }
    public function delete($id){
        $this->supplier_m->delete($id);
        redirect('admin/supplier');
    }
}


