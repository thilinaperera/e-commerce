<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */

class User extends Admin_Controller{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        //$this->data['users'] = $this->user_m->get();
        $this->data['users'] = $this->user_m->get_with_type();
        //var_dump( $this->data['users'] );


        $this->data['subview'] = 'admin/user/index';
        $this->load->view('admin/_layout_main',$this->data);

    }
    public function edit($id = null){
        if($id){
            $this->data['user'] = $this->user_m->get($id);
            if(!count($this->data['user'])){
                $this->data['errors'][] = "User could not be found";
                var_dump($this->data);
            }
        }
        else{
            // this will return empty user details
            $this->data['user'] = $this->user_m->get_new();
        }
        //var_dump($this->data);
        $rules = $this->user_m->rules_admin ;
        if($id != null){
            $rules['password']['rules'] .= '|required';
        }


        // user types for dropdown
        $this->data['user_types'] = $this->user_m->get_user_types();

        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            // ok please save this data
            $data = $this->user_m->array_from_post(array('name','email','password','usertype_id'));
            $data['password'] = $this->user_m->hash($data['password']);
            $this->user_m->save($data, $id);
            redirect('admin/user');

        }
        $this->data['subview'] = 'admin/user/edit';
        $this->load->view('admin/_layout_main',$this->data);

    }
    public function delete($id){
        $this->user_m->delete($id);
        redirect('admin/user');
    }
    public function login(){

        $dashboard = 'admin/dashboard';
        $memberDashboard = 'member';

        if($this->user_m->loggedin()){
            // send logged user to dashboard
            if($this->user_m->is_member()){
                redirect($memberDashboard);
            }else{
               //redirect($dashboard)
                redirect($dashboard);
            }

        }


        $rules = $this->user_m->rules ;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            if( $this->user_m->login() == true ){
                // user successfully logged in
                redirect($dashboard);
            }
            else{
                $this->session->set_flashdata('error','That email/password combination does not match or not exist');
                redirect('admin/user/login');
            }
        }

        $this->data['subview'] = 'admin/user/login';
        $this->load->view('admin/_layout_modal',$this->data);

    }
    public function logout(){
        $this->user_m->logout();
        redirect('admin/user/login');
    }
    public function _unique_email($str){

        $id = $this->uri->segment(4);
        $this->db->where('email',$this->input->post('email'));
        !$id || $this->db->where('id !=', $id);
        $user = $this->user_m->get();

        if(count($user)){
            $this->form_validation->set_message('_unique_email','%s should be unique');
            return false;
        }
        return true;
    }

}


