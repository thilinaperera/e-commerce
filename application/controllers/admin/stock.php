<?php
/**
 * Created by PhpStorm.
 * stock: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */

class Stock extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('stock_m');
    }

    public function index(){
        $this->data['stocks'] = $this->stock_m->get();
        $this->data['subview'] = 'admin/stock/index';
        $this->load->view('admin/_layout_main',$this->data);
        //var_dump($this->data['stocks']);
    }
    public function edit($id = null){
        if($id){
            $this->data['stock'] = $this->stock_m->get($id);
            if(!count($this->data['stock'])){
                $this->data['errors'][] = "stock could not be found";
            }
        }
        else{
            // this will return empty stock details
            $this->data['stock'] = $this->stock_m->get_new();

        }

        $rules = $this->stock_m->rules ;
        // products for dropdown
        $this->data['suppliers'] = $this->stock_m->get_with_suppliers();
        $this->data['products'] = $this->stock_m->get_with_products();
        $this->form_validation->set_rules($rules);

        if($this->form_validation->run()==true){
           // ok please save this data
            $data = $this->stock_m->array_from_post(array('description','in_date','in_quantity','unit_price','supplier_id','product_id'));
            $this->stock_m->save($data, $id);
            redirect('admin/stock');

        }
        $this->data['subview'] = 'admin/stock/edit';
        $this->load->view('admin/_layout_main',$this->data);


    }
    public function delete($id){
        $this->stock_m->delete($id);
        redirect('admin/stock');
    }

}


