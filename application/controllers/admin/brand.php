<?php
/**
 * Created by PhpStorm.
 * brand: Thilina
 * Date: 5/17/14
 * Time: 8:32 AM
 */

class Brand extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('brand_m');
    }

    public function index(){
        $this->data['brands'] = $this->brand_m->get();
        $this->data['subview'] = 'admin/brand/index';
        $this->load->view('admin/_layout_main',$this->data);
        //var_dump($this->data['brands']);
    }
    public function edit($id = null){
        if($id){
            $this->data['brand'] = $this->brand_m->get($id);
            if(!count($this->data['brand'])){
                $this->data['errors'][] = "brand could not be found";
            }
        }
        else{
            // this will return empty brand details
            $this->data['brand'] = $this->brand_m->get_new();
        }
       // var_dump($this->data['brand']);
        $rules = $this->brand_m->rules ;


        $this->form_validation->set_rules($rules);
        if($this->form_validation->run()==true){
            // ok please save this data
            $data = $this->brand_m->array_from_post(array('name','description'));
            $this->brand_m->save($data, $id);
            redirect('admin/brand');

        }
        $this->data['subview'] = 'admin/brand/edit';
        $this->load->view('admin/_layout_main',$this->data);


    }
    public function delete($id){
        $this->brand_m->delete($id);
        redirect('admin/brand');
    }
}


