<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/28/14
 * Time: 4:01 PM
 */

class Shopping extends Member_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('member_m');
        $this->load->model('product_m');
        $this->load->model('shopping_m');
        $this->load->model('cart_m');

    }
    public function add(){

        $data = $this->product_m->array_from_post(array('id','name','qty','options','price'));
        $this->data['product'] = $this->product_m->get_with_price_one($data['id']);
        //var_dump($this->data['product'],$data);
        $data = array(
            'id'      => $this->data['product']->id,
            'qty'     => (float) $data['qty'],
            'price'   => $this->data['product']->price,
            'name'    =>$this->data['product']->name,
        );
        //var_dump( $data['qty']);
        $this->cart->insert($data);
        redirect('member');
    }
    public function cart(){
        //echo json_encode($this->cart->contents());
        $this->data['subview'] = 'member/subviews/cart';
        $this->data['sidebar'] = 'member/subviews/sidebar';
        $this->load->view('member/_layout_main',$this->data);
        //var_dump( $this->shopping_m->get_all_orders());
        //var_dump( $this->shopping_m->get_all_complete_orders());
        //var_dump( $this->shopping_m->get_all_rejected_orders());
    }
    public function checkout(){
        $this->data['order'] = $this->shopping_m->get_new();

        $rules = $this->shopping_m->rules ;
        $this->form_validation->set_rules($rules);

        if( $this->cart->contents() ){
            if($this->form_validation->run()==true){
                $data = $this->member_m->array_from_post(array('shipping_name_first','shipping_name_last','shipping_address1','shipping_address2','billing_name_first','billing_name_last','billing_address1','billing_address2'));
                $data['status'] = 'pending';
                $data['user_id'] = (int) $this->session->userdata['id'];
                date_default_timezone_get('Asia/Colombo');
                $data['date'] =  date('y-m-d h:i:s a', time());
                $get_id = $this->shopping_m->save($data);
                $this->session->set_flashdata('success','Order successfully send for approval');

                // save cart data
                foreach( $this->cart->contents() as $item){
                    $array = array(
                        'row_id' => $item['rowid'],
                        'name' => $item['name'],
                        'product_id' => $item['id'],
                        'order_id' => $get_id,
                        'qty' => $item['qty'],
                        'price' => $item['price'],
                        'subtotal' => $item['subtotal'],
                    );
                    $this->cart_m->save($array);
                }

                $this->cart->destroy();
                redirect('member');
                //var_dump($data);

            }
        }
        else{
            $this->session->set_flashdata('success','Sorry cart is empty');
            //redirect('member');
        }





        $this->data['subview'] = 'member/subviews/checkout';
        $this->data['sidebar'] = 'member/subviews/sidebar';
        $this->load->view('member/_layout_main',$this->data);
    }

    public function order(){

        $user_id = $this->session->userdata("id");
        $this->data['orders'] = $this->shopping_m->get_user_orders($user_id);
        $this->data['subview'] = 'member/subviews/order';
        $this->data['sidebar'] = 'member/subviews/sidebar';
        $this->load->view('member/_layout_main',$this->data);
    }
    public function set(){
        $this->shopping_m->set_complete_status(15);
    }
    public function remove($id = null){
        if($id){
            $this->cart->update(array(
                'rowid' => $id,
                'qty' => 0,
            ));
            redirect('shopping/cart');
        }
    }
} 