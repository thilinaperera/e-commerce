<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 12:05 PM
 */

class Migration_Create_stocks extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ),
            'product_id' => array(
                'type' => 'INT',
                'constraint' => 5,
            ),
            'description' => array(
                'type' => 'TEXT',
            ),
            'in_date' => array(
                'type' => 'DATE',
            ),
            'in_quantity' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
            'unit_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
            'supplier_id' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
        ));
        $this->dbforge->add_key('id',true);
        $this->dbforge->create_table('stocks');
    }

    public function down()
    {
        $this->dbforge->drop_table('stocks');
    }
}
