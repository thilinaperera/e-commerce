<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/22/14
 * Time: 9:11 AM
 */

class Migration_Usertype_id_to_users extends CI_Migration {

    public function up()
    {
            $fields =  (array(
            'usertype_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'default' => 0
            ),
        ));
        $this->dbforge->add_column('users',$fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('users','usertype_id');
    }
}
