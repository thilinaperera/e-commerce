<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 12:05 PM
 */

class Migration_Create_returns extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ),
            'comment' => array(
                'type' => 'TEXT',
            ),
            'date' => array(
                'type' => 'DATE',
            ),
            'quantity' => array(
                'type' => 'DECIMAL',
                'constraint' => '5,2',
            ),
            'stock_id' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
            'issued_id' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
            'product_id' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
        ));
        $this->dbforge->add_key('id',true);
        $this->dbforge->create_table('returns');
    }

    public function down()
    {
        $this->dbforge->drop_table('returns');
    }
}
