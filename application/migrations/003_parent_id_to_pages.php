<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/22/14
 * Time: 9:11 AM
 */

class Migration_Parent_id_to_pages extends CI_Migration {

    public function up()
    {
            $fields =  (array(
            'parent_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'default' => 0
            ),
        ));
        $this->dbforge->add_column('pages',$fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('pages','parent_id');
    }
}
