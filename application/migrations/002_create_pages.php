<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 12:05 PM
 */

class Migration_Create_pages extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'order' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
            'body' => array(
                'type' => 'TEXT',
            )
        ));
        $this->dbforge->add_key('id',true);
        $this->dbforge->create_table('pages');
    }

    public function down()
    {
        $this->dbforge->drop_table('pages');
    }
}
