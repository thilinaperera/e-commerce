<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 12:05 PM
 */

class Migration_Create_suppliers extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'phone' => array(
                'type' => 'INT',
                'constraint' => '10',
            ),

            'description' => array(
                'type' => 'TEXT',
            ),
        ));
        $this->dbforge->add_key('id',true);
        $this->dbforge->create_table('suppliers');
    }

    public function down()
    {
        $this->dbforge->drop_table('suppliers');
    }
}
