<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 12:05 PM
 */

class Migration_Create_carts extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ),
            'row_id' => array(
                'type' => 'INT',
                'constraint' => '20',
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
            'product_id' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
            'qty' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
            'price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
            'subtotal' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            )

        ));
        $this->dbforge->add_key('id',true);
        $this->dbforge->create_table('carts');
    }

    public function down()
    {
        $this->dbforge->drop_table('carts');
    }
}
