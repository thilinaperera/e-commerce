<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 12:05 PM
 */

class Migration_Create_products_issued_stocks extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ),
            'stock_id' => array(
                'type' => 'INT',
                'constraint' => 5,
            ),
            'product_id' => array(
                'type' => 'INT',
                'constraint' => 5,
            ),
            'date_issued' => array(
                'type' => 'DATE',
            ),
            'quantity_issued' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
            'expiry_date' => array(
                'type' => 'DATE',
            )
        ));
        $this->dbforge->add_key('id',true);
        $this->dbforge->create_table('products_issued_stocks');
    }

    public function down()
    {
        $this->dbforge->drop_table('products_issued_stocks');
    }
}
