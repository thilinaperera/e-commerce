<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 12:05 PM
 */

class Migration_Create_orders extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => TRUE
            ),
            'shipping_name_first' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'shipping_name_last' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'shipping_address1' => array(
                'type' => 'TEXT',
            ),
            'shipping_address2' => array(
                'type' => 'TEXT',
            ),
            'billing_name_first' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'billing_name_last' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'billing_address1' => array(
                'type' => 'TEXT',
            ),
            'billing_address2' => array(
                'type' => 'TEXT',
            ),
            'date' => array(
                'type' => 'DATETIME',
            ),

            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => '5',
            )
        ));
        $this->dbforge->add_key('id',true);
        $this->dbforge->create_table('orders');
    }

    public function down()
    {
        $this->dbforge->drop_table('orders');
    }
}
