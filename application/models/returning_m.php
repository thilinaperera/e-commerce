<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Returning_m extends MY_Model{
    protected $_table_name = 'returns';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'date';
    public  $rules_stock = array(
        'stock_id' => array(
            'field' => 'stock_id',
            'label' => 'Stock',
            'rules' => 'trim|intval|required'
        ),
        'product_id' => array(
            'field' => 'product_id',
            'label' => 'Product',
            'rules' => 'trim|intval'
        ),
        'date' => array(
            'field' => 'date',
            'label' => 'Date',
            'rules' => 'trim|required|exact_length[10]|xss_clean'
        ),
        'quantity' => array(
            'field' => 'quantity',
            'label' => 'Quantity',
            'rules' => 'trim|intval|required'
        ),
        'comment' => array(
            'field' => 'comment',
            'label' => 'Comment',
            'rules' => 'trim'
        )
    );
    public  $rules_issued = array(
        'issued_id' => array(
            'field' => 'return_id',
            'label' => 'return',
            'rules' => 'trim|intval|required'
        ),
        'stock_id' => array(
            'field' => 'return_id',
            'label' => 'return',
            'rules' => 'trim|intval'
        ),
        'product_id' => array(
            'field' => 'product_id',
            'label' => 'return',
            'rules' => 'trim|intval'
        ),
        'date' => array(
            'field' => 'date',
            'label' => 'Date',
            'rules' => 'trim|required|exact_length[10]|xss_clean'
        ),
        'quantity' => array(
            'field' => 'quantity',
            'label' => 'Quantity',
            'rules' => 'trim|intval|required'
        ),
        'comment' => array(
            'field' => 'comment',
            'label' => 'Comment',
            'rules' => 'trim'
        )
    );

    // get returns without stock issues
    public function get_without_stock_returns($id = null , $single = false){
        $this->db->select('returns.*');
        $this->db->where('returns.issued_id != 0');
        return parent::get($id, $single);
    }

    // get all with product names
    public function get_with_product_names($id = null , $single = false){
        $this->db->select('returns.*, p.name as product');
        $this->db->join('products as p','returns.product_id = p.id','left');
        return parent::get($id, $single);
    }

    // get product id for issued tabale
    public function get_product_form_issued($id = null){
        $this->db->select('products_issued_stocks.product_id');
        $this->db->where('id',$id);
        $products = $this->db->get('products_issued_stocks');

        if(count($products->result())){
            if($products->num_rows() == 1){
                return  $products->result()[0]->product_id;
            }
        }

        return null ;
    }

    // get return item with products
    public function get_with_products(){
        $this->db->select('products.id,products.name');
        $products = $this->db->get('products');
        $array = array();
        if(count($products->result())){
            foreach($products->result() as $product){
                $array[$product->id] = $product->name;
            }
        }
        //var_dump($array );
        return $array;
    }
    // get return item with returns
    public function get_with_returns(){
        $this->db->select('returns.id,returns.description');
        $returnings = $this->db->get('returns');
        $array = array();
        if(count($returnings->result())){
            foreach($returnings->result() as $returning){
                $array[$returning->id] = $returning->description;
            }
        }
        //var_dump($array );
        return $array;
    }
    public function get_with_issues(){

        $this->db->select('products_issued_stocks.id,products_issued_stocks.date_issued,p.name');
        $this->db->join('products as p','products_issued_stocks.product_id = p.id','left');
        $products = $this->db->get('products_issued_stocks');
        $array = array();
        if(count($products->result())){
            foreach($products->result() as $product){
                $array[$product->id] = ''.$product->id.'  -  ( '.$product->date_issued.' ) ' .$product->name ;
            }
        }
        //var_dump($array );
        return $array;
    }
/*
    public function get_with_type($id = null , $single = false){
        $this->db->select('users.*, p.type as usertype');
        $this->db->join('usertypes as p','users.usertype_id = p.id','left');
        return parent::get($id, $single);
    }

*/

    // get items from stock
    public function get_with_stocks(){
        $this->db->select('stocks.id,stocks.description,stocks.in_date');
        $stocks = $this->db->get('stocks');
        $array = array();
        if(count($stocks->result())){
            foreach($stocks->result() as $stock){
                $array[$stock->id] = $stock->id.' - ( '.$stock->in_date.' ) '.$stock->description;
            }
        }
        //var_dump($array );
        return $array;
    }

    public function get_with_status(){
        $array = array();
        $array[0] = 'Return';
        $array[1] = 'Issued';
        return $array;
    }

    public function total_returns_form_issued($issued_id = null){
        $this->db->select('quantity');
        $this->db->where('issued_id',$issued_id);
        $quantities = $this->db->get('returns');
        $total = null;
        if(count($quantities->result())){
            foreach($quantities->result() as $quantity ){
                $total = $total + (float) $quantity->quantity;
            }
        }
        else{
            $total = null;
        }
        return $total;
    }

    public function get_current_issued_quantity($issued_id = null){
        $this->db->select('quantity_issued');
        $this->db->where('id',$issued_id);
        $quantities = $this->db->get('products_issued_stocks');
        $total = null;
        if(count($quantities->result())){
            foreach($quantities->result() as $quantity ){
                $total = $total + (float) $quantity->quantity_issued;
            }
        }
        else{
            $total = null;
        }
        return $total;
    }

    public function get_return_amount($stock_id = null, $single = false){
        $this->db->select('quantity');
        $this->db->where('stock_id',$stock_id);
        $quantities = $this->db->get('returns');
        $total = null;
        if(count($quantities->result())){
            foreach($quantities->result() as $quantity ){
                $total = $total + (float) $quantity->quantity;
            }
        }
        else{
            $total = null;
        }
        //return parent::get($id, $single);
        return $total;
    }

    public function get_old_issued_amount($stock_id = null, $id = null , $single = false){
        $this->db->select('quantity_issued');
        $this->db->where('stock_id',$stock_id);

        if($id){
            $this->db->where('id !=',$id);
        }
        else{

        }

        $quantities = $this->db->get('products_issued_stocks');
        $total = null;
        if(count($quantities->result())){
            foreach($quantities->result() as $quantity ){
                $total = $total + (float) $quantity->quantity_issued;
            }
        }
        else{
            $total = null;
        }
        //return parent::get($id, $single);
        return $total;
    }

    public function get_stock_quantity($stock_id = null){
        $this->db->select('in_quantity');
        $this->db->where('id',$stock_id);
        $quantities = $this->db->get('stocks');
        $total = null;
        if(count($quantities->result())){
            foreach($quantities->result() as $quantity ){
                $total = $total + (float) $quantity->in_quantity;
            }
        }
        else{
            $total = null;
        }
        return $total;
    }


    public function get_product_id($stock_id = null){
        $this->db->select('product_id');
        $this->db->where('id',$stock_id);
        $products = $this->db->get('stocks');
        $total = null;
        if(count($products->result())){
            foreach($products->result() as $product ){
                $total = (int) $product->product_id;
            }
        }
        else{
            $total = null;
        }
        return $total;
    }

    public function get_new(){
        $returning = new stdClass();
        $returning->stock_id = null;
        $returning->product_id = '';
        $returning->issued_id = null;
        $returning->date = date('Y-m-d');
        $returning->quantity = 0.00;
        $returning->comment = '';
        return $returning;
    }
}