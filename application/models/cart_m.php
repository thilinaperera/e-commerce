<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Cart_m extends MY_Model{
    protected $_table_name = 'carts';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'date';
}