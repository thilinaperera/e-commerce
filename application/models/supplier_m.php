<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Supplier_m extends MY_Model{
    protected $_table_name = 'suppliers';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'name';
    public  $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Name' ,
            'rules' => 'trim|required|xss_clean'
        ),
        'email' => array(
            'field' => 'email',
            'label' => 'Email' ,
            'rules' => 'trim|required|valid_email|xss_clean'
        ),
        'phone' => array(
            'field' => 'phone',
            'label' => 'Phone' ,
            'rules' => 'trim|required|intval'
        ),
        'Description' => array(
            'field' => 'description',
            'label' => 'Description' ,
            'rules' => 'trim'
        )
    );

    public function get_new(){
        $supplier = new stdClass();
        $supplier->name = '';
        $supplier->email = '';
        $supplier->phone = '';
        $supplier->description = '';
        return $supplier;
    }

}