<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Page_m extends MY_Model{
    protected $_table_name = 'pages';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'order';
    public  $rules = array(
        'parent_id' => array(
            'field' => 'parent_id',
            'label' => 'Parent',
            'rules' => 'trim|intval'
        ),
        'title' => array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required|max_length[100]|xss_clean'
        ),
        'slug' => array(
            'field' => 'slug',
            'label' => 'Slug',
            'rules' => 'trim|required|ur_title|max_length[100]|callback__unique_slug|xss_clean'
        ),
        'order' => array(
            'field' => 'order',
            'label' => 'Order',
            'rules' => 'trim|required|is_natural|xss_clean'
        ),
        'body' => array(
            'field' => 'body',
            'label' => 'Body',
            'rules' => 'trim|required'
        )
    );

    public function get_new(){
        $page = new stdClass();
        $page->title = '';
        $page->slug = '';
        $page->order = '';
        $page->body = '';
        $page->parent_id = 0;
        return $page;
    }

    public function delete($id){
        // delete a page
        parent::delete($id);
        // reset parent id for itd children
        $this->db->set(array('parent_id' =>0 ))->where('parent_id',$id)->update($this->_table_name);

    }

    public function get_with_parent($id = null , $single = false){
        $this->db->select('pages.*, p.slug as parent_slug, p.title as parent_title');
        $this->db->join('pages as p','pages.parent_id = p.id','left');
        return parent::get($id, $single);
    }

    public function get_no_parents(){
        $this->db->select('id,title');
        $this->db->where('parent_id',0);
        $pages = parent::get();
        $array = array(0 => 'No parent');
        if(count($pages)){
            foreach($pages as $page){

                $array[$page->id] = $page->title;

            }
        }
        return $array;
    }

}