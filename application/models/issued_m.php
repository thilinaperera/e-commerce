<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Issued_m extends MY_Model{
    protected $_table_name = 'products_issued_stocks';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'date_issued';
    public  $rules = array(
        'stock_id' => array(
            'field' => 'stock_id',
            'label' => 'Stock',
            'rules' => 'trim|intval|required'
        ),
        'product_id' => array(
            'field' => 'product_id',
            'label' => 'Product',
            'rules' => 'trim|intval'
        ),
        'date_issued' => array(
            'field' => 'date_issued',
            'label' => 'Date issued',
            'rules' => 'trim|required|exact_length[10]|xss_clean'
        ),
        'quantity_issued' => array(
            'field' => 'quantity_issued',
            'label' => 'Quantity issued',
            'rules' => 'trim|intval|required'
        ),
        'expiry_date' => array(
            'field' => 'expiry_date',
            'label' => 'Expiry date',
            'rules' => 'trim|required|exact_length[10]|xss_clean'
        )
    );
    // get stock item with products
    public function get_with_products(){
        $this->db->select('products.id,products.name');
        $products = $this->db->get('products');
        $array = array();
        if(count($products->result())){
            foreach($products->result() as $product){
                $array[$product->id] = $product->name;
            }
        }
        //var_dump($array );
        return $array;
    }
    // get stock item with stocks
    public function get_with_stocks(){
        $this->db->select('stocks.id,stocks.description');
        $stocks = $this->db->get('stocks');
        $array = array();
        if(count($stocks->result())){
            foreach($stocks->result() as $stock){
                $array[$stock->id] = $stock->description;
            }
        }
        //var_dump($array );
        return $array;
    }

    public function get_with_status(){
        $array = array();
        $array[0] = 'Return';
        $array[1] = 'Issued';
        return $array;
    }

    public function get_new(){
        $issued = new stdClass();
        $issued->stock_id = '';
        $issued->product_id = '';
        $issued->date_issued = date('Y-m-d');
        $issued->quantity_issued = 0;
        $issued->expiry_date = date('Y-m-d');
        return $issued;
    }


    public function get_return_amount($stock_id = null, $single = false){
        $this->db->select('quantity');
        $this->db->where('stock_id',$stock_id);
        $quantities = $this->db->get('returns');
        $total = null;
        if(count($quantities->result())){
            foreach($quantities->result() as $quantity ){
                $total = $total + (float) $quantity->quantity;
            }
        }
        else{
            $total = null;
        }
        //return parent::get($id, $single);
        return $total;
    }
    public function get_old_issued_amount($stock_id = null, $id = null , $single = false){
        $this->db->select('quantity_issued');
        $this->db->where('stock_id',$stock_id);

        if($id){
            $this->db->where('id !=',$id);
        }
        else{

        }

        $quantities = $this->db->get('products_issued_stocks');
        $total = null;
        if(count($quantities->result())){
            foreach($quantities->result() as $quantity ){
                $total = $total + (float) $quantity->quantity_issued;
            }
        }
        else{
            $total = null;
        }
        //return parent::get($id, $single);
        return $total;
    }

    public function get_product_id($stock_id = null){
        $this->db->select('product_id');
        $this->db->where('id',$stock_id);
        $products = $this->db->get('stocks');
        $total = null;
        if(count($products->result())){
            foreach($products->result() as $product ){
                $total = (int) $product->product_id;
            }
        }
        else{
            $total = null;
        }
        return $total;
    }

    public function get_stock_quantity($stock_id = null){
        $this->db->select('in_quantity');
        $this->db->where('id',$stock_id);
        $quantities = $this->db->get('stocks');
        $total = null;
        if(count($quantities->result())){
            foreach($quantities->result() as $quantity ){
                $total = $total + (float) $quantity->in_quantity;
            }
        }
        else{
            $total = null;
        }
        return $total;
    }

    public function get_with_product_names(){

        $this->db->select('products_issued_stocks.*, p.name as name, p.description as description');
        $this->db->join('products as p','products_issued_stocks.product_id = p.id','left');
        return parent::get();

    }




}