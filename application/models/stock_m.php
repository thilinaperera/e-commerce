<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Stock_m extends MY_Model{
    protected $_table_name = 'stocks';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'in_date';
    public  $rules = array(
        'supplier_id' => array(
            'field' => 'supplier_id',
            'label' => 'Supplier',
            'rules' => 'trim|intval|required'
        ),
        'product_id' => array(
            'field' => 'product_id',
            'label' => 'product',
            'rules' => 'trim|intval|required'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim'
        ),
        'in_date' => array(
            'field' => 'in_date',
            'label' => 'Stock in date',
            'rules' => 'trim|required|exact_length[10]|xss_clean'
        ),
        'in_quantity' => array(
            'field' => 'in_quantity',
            'label' => 'Stock in quantity',
            'rules' => 'trim|intval|required'
        ),
        'unit_price' => array(
            'field' => 'unit_price',
            'label' => 'Unit price',
            'rules' => 'trim|decimal|required'
        )
    );

    // get stock item with products
    public function get_with_products(){
        $this->db->select('products.id,products.name');
        $products = $this->db->get('products');
        $array = array();
        if(count($products->result())){
            foreach($products->result() as $product){
                $array[$product->id] = $product->name;
            }
        }
        //var_dump($array );
        return $array;
    }

    // get stock item with suppliers
    public function get_with_suppliers(){
        $this->db->select('suppliers.id,suppliers.name');
        $suppliers = $this->db->get('suppliers');
        $array = array();
        if(count($suppliers->result())){
            foreach($suppliers->result() as $supplier){
                $array[$supplier->id] = $supplier->name;
            }
        }
        //var_dump($array );
        return $array;
    }
    public function get_new(){
        $stock = new stdClass();
        $stock->description = '';
        $stock->in_date = date('Y-m-d');
        $stock->in_quantity = 0;
        $stock->unit_price = 0;
        $stock->supplier_id = '';
        $stock->product_id = '';
        return $stock;
    }



}