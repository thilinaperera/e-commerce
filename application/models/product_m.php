<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Product_m extends MY_Model{
    protected $_table_name = 'products';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'name';
    public  $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|max_length[100]|xss_clean'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required|xss_clean'
        ),
        'category_id' => array(
            'field' => 'category_id',
            'label' => 'Category',
            'rules' => 'trim|intval|required'
        ),
        'brand_id' => array(
            'field' => 'brand_id',
            'label' => 'Brand',
            'rules' => 'trim|intval|required'
        ),
        'image' => array(
            'field' => 'image',
            'label' => 'Image',
            'rules' => 'trim'
        )
    );

    public function get_new(){
        $product = new stdClass();
        $product->name = '';
        $product->description = '';
        $product->category_id = '';
        $product->brand_id = '';
        $product->image = '';
        return $product;
    }
/*
    public function get_with_category($id = null , $single = false){
        $this->db->select('products.*, c.name as category');
        $this->db->join('categories as c','products.category_id = c.id','left');
        return parent::get($id, $single);
    }
*/
    // get all categories and add it to array
    public function get_with_categories(){
        $this->db->select('categories.id,categories.name');
        $categories = $this->db->get('categories');
        $array = array();
        if(count($categories->result())){
            foreach($categories->result() as $category){
                $array[$category->id] = $category->name;
            }
        }
        //var_dump($array );
        return $array;
    }
    public function get_with_brands(){
        $this->db->select('brands.id,brands.name');
        $brands = $this->db->get('brands');
        $array = array();
        if(count($brands->result())){
            foreach($brands->result() as $brand){
                $array[$brand->id] = $brand->name;
            }
        }
        //var_dump($array );
        return $array;
    }

    // get products with brand name and category name ( join categories,brand tables with products table )
    public function get_cat_and_brand($id = null, $single = false){
        $this->db->select('products.*, b.name as brand, c.name as category');
        $this->db->join('brands as b','products.brand_id = b.id','left');
        $this->db->join('categories as c','products.category_id = c.id','left');
        return parent::get($id, $single);
    }



    /*
        public function delete($id){
            // delete a product
            parent::delete($id);
            // reset parent id for itd children
            $this->db->set(array('category_id' =>0))->where('category_id',$id)->update($this->_table_name);
            $this->db->set(array('brand_id'=>0 ))->where('category_id',$id)->update($this->_table_name);

        }
    */

    // cart functions
/*
SELECT products.name, products_issued_stocks.date_issued
FROM `products`
INNER JOIN `products_issued_stocks` on products.id = products_issued_stocks.product_id;
*/
    public function get_with_price($id = null , $single = false){
       $this->db->select('products.*, s.unit_price as price');
       $this->db->join('stocks as s','products.id = s.product_id');
       return parent::get($id, $single);
    }
    public function get_with_price_one($id = null, $single = false){
       $this->db->select('products.*, stocks.unit_price as price');
       $this->db->from('products, stocks');
       $this->db->where('products.id',$id);
       $this->db->where('stocks.product_id',$id);
       $datas  =  $this->db->get();
        foreach ($datas->result() as $data)
        {
            return $data;

        }
        return null;
    }
/*
    public function get_cat_and_brand($id = null, $single = false){
        $this->db->select('products.*, b.name as brand, c.name as category');
        $this->db->join('brands as b','products.brand_id = b.id','left');
        $this->db->join('categories as c','products.category_id = c.id','left');
        return $this->db->get();
    }
*/
}