<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Shopping_m extends MY_Model{
    protected $_table_name = 'orders';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'date';
    public  $rules = array(
        'shipping_name_first' => array(
            'field' => 'shipping_name_first',
            'label' => 'Shipping first Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'shipping_name_last' => array(
            'field' => 'shipping_name_last',
            'label' => 'Shipping Last Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'shipping_address1' => array(
            'field' => 'shipping_address1',
            'label' => 'Shipping Address 1',
            'rules' => 'trim|required|xss_clean'
        ),
        'shipping_address2' => array(
            'field' => 'shipping_address2',
            'label' => 'Shipping Address 2',
            'rules' => 'trim|required|xss_clean'
        ),
        'billing_name_first' => array(
            'field' => 'billing_name_first',
            'label' => 'Billing first Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'billing_name_last' => array(
            'field' => 'billing_name_last',
            'label' => 'Billing Last Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'billing_address1' => array(
            'field' => 'billing_address1',
            'label' => 'Billing Address 1',
            'rules' => 'trim|required|xss_clean'
        ),
        'billing_address2' => array(
            'field' => 'billing_address2',
            'label' => 'Billing Address 2',
            'rules' => 'trim|required|xss_clean'
        ),
    );
    public  $rules_admin = array(
        'status' => array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim|required|xss_clean'
        ),
        'user_id' => array(
            'field' => 'user_id',
            'label' => 'User',
            'rules' => 'trim|required|xss_clean'
        ),
        'shipping_name_first' => array(
            'field' => 'shipping_name_first',
            'label' => 'Shipping first Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'shipping_name_last' => array(
            'field' => 'shipping_name_last',
            'label' => 'Shipping Last Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'shipping_address1' => array(
            'field' => 'shipping_address1',
            'label' => 'Shipping Address 1',
            'rules' => 'trim|required|xss_clean'
        ),
        'shipping_address2' => array(
            'field' => 'shipping_address2',
            'label' => 'Shipping Address 2',
            'rules' => 'trim|required|xss_clean'
        ),
        'billing_name_first' => array(
            'field' => 'billing_name_first',
            'label' => 'Billing first Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'billing_name_last' => array(
            'field' => 'billing_name_last',
            'label' => 'Billing Last Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'billing_address1' => array(
            'field' => 'billing_address1',
            'label' => 'Billing Address 1',
            'rules' => 'trim|required|xss_clean'
        ),
        'billing_address2' => array(
            'field' => 'billing_address2',
            'label' => 'Billing Address 2',
            'rules' => 'trim|required|xss_clean'
        ),
        'date' => array(
            'field' => 'date',
            'label' => 'Date',
            'rules' => 'trim|required|exact_length[10]|xss_clean'
        ),
    );

    public function get_new(){
        $order = new stdClass();
        $order->user_id = '';
        $order->status = '';
        $order->shipping_name_first = '';
        $order->shipping_name_last = '';
        $order->shipping_address1 = '';
        $order->shipping_address2 = '';
        $order->billing_name_first = '';
        $order->billing_name_last = '';
        $order->billing_address1 = '';
        $order->billing_address2 = '';
        $order->date = '';
        return $order;
    }



    public function get_all_orders(){
        $this->db->select('*');
        return parent::get();
    }



     public function get_cart($id = null , $single = false){
        $this->db->select('orders.*, order.* ');
        $this->db->join('orders as order','users.usertype_id = orderp.id','left');
        return parent::get($id, $single);
    }

    public function get_all_order_cart($id = null){
        $this->db->select('carts.*');
        $this->db->where('order_id', $id);
        $results = $this->db->get('carts')->result_array();
        return $results;
    }
    public function get_all_complete_orders(){
        $this->db->select('*');
        $this->db->where('status','complete');
        return parent::get();
    }
    public function get_all_pending_orders(){
        $this->db->select('*');
        $this->db->where('status','pending');
        return parent::get();
    }
    public function get_last_pending_order(){
        $this->db->select('*');
        $this->db->where('status','pending');
        $results = $this->db->get('orders')->last_row();
        //var_dump($results->id);

        $this->db->select('*');
        $this->db->where('order_id',$results->id);
        $cart = $this->db->get('carts')->result_array();
        //var_dump($cart);

        return $cart;
    }
    public function get_all_rejected_orders(){
        $this->db->select('*');
        $this->db->where('status','rejected');
        return parent::get();
    }
    public function get_from_search_orders(){
        $this->db->select('*');
        $this->db->where('status','rejected');
        return parent::get();
    }
    public function set_complete_status($id = null){
        $this->db->set('status','complete');
        $this->db->where('id',$id);
        return $this->db->update('orders');
    }
    public function set_status($id = null,$status = null){
        $this->db->set('status',$status);
        $this->db->where('id',$id);
        return $this->db->update('orders');
    }
    public function get_user_orders($id = null){
            $this->db->select('orders.*');
            $this->db->where('orders.user_id',$id);
            return parent::get();

    }

}