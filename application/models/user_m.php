<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/17/14
 * Time: 9:31 AM
 */
class User_m extends MY_Model{
    protected $_table_name = 'users';
    protected $_order_by = 'name';
    public  $rules = array(
        'email' => array('field' => 'email', 'label' => 'Email' , 'rules' => 'trim|required|valid_email|xss_clean'),
        'password' => array('field' => 'password', 'label' => 'Password' , 'rules' => 'trim|required'),
    );
    public  $rules_user = array(
        'usertype_id' => array(
            'field' => 'usertype_id',
            'label' => 'Role',
            'rules' => 'trim|intval'
        ),
        'name' => array(
            'field' => 'name',
            'label' => 'Name' ,
            'rules' => 'trim|required|xss_clean'
        ),
        'email' => array(
            'field' => 'email',
            'label' => 'Email' ,
            'rules' => 'trim|required|valid_email|xss_clean'
        ),
        'password' => array(
            'field' => 'password',
            'label' => 'Password' ,
            'rules' => 'trim|matches[password_confirm]'
        ),
        'password_confirm' => array(
            'field' => 'password_confirm',
            'label' => 'Confirm Password' ,
            'rules' => 'trim|matches[password]'
        ),
    );
    public  $rules_admin = array(
        'usertype_id' => array(
            'field' => 'usertype_id',
            'label' => 'Role',
            'rules' => 'trim|intval'
        ),
        'name' => array(
            'field' => 'name',
            'label' => 'Name' ,
            'rules' => 'trim|required|xss_clean'
        ),
        'email' => array(
            'field' => 'email',
            'label' => 'Email' ,
            'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean'
        ),
        'password' => array(
            'field' => 'password',
            'label' => 'Password' ,
            'rules' => 'trim|matches[password_confirm]'
        ),
        'password_confirm' => array(
            'field' => 'password_confirm',
            'label' => 'Confirm Password' ,
            'rules' => 'trim|matches[password]'
        ),
    );
    function __construct (){
        parent::__construct();
    }
    public function login(){
        $user = $this->get_by(array(
            'email' => $this->input->post('email'),
            'password' => $this->hash($this->input->post('password'))
        ),true);
        //var_dump($user);
        if(count($user)){
            $data = array(
                'name' => $user->name,
                'email' => $user->email,
                'id' => $user->id,
                'usertype' => $this->get_type_name($user->usertype_id),
                'loggedin' => true
            );
            $this->session->set_userdata($data);
        }
    }
    public function logout(){
        $this->session->sess_destroy();
    }

    public function get_with_type($id = null , $single = false){
        $this->db->select('users.*, p.type as usertype');
        $this->db->join('usertypes as p','users.usertype_id = p.id','left');
        return parent::get($id, $single);
    }

    public function get_user_types(){
        $this->db->select('usertypes.id,type');
        $this->db->from('usertypes');
        $types = parent::get();
        $array = array();
        if(count($types)){
            foreach($types as $type){

                $array[$type->id] = $type->type;

            }
        }
        return $array;
    }

    public function get_type_name($id = null , $single = false){

        $this->db->select('usertypes.type');
        $this->db->where('usertypes.id',$id);
        $result = $this->db->get('usertypes');
        if($result->num_rows == 1){
            return $result->row('type');
        }

    }

    public function get_new(){
        $user = new stdClass();
        $user->name = '';
        $user->usertype_id = '';
        $user->email = '';
        $user->password = '';
        return $user;

    }

    // check user type is member
    public function is_member(){
        if($this->session->userdata('usertype') == 'member'){
            return true;
        }
        else{
            return false;
        }
    }

    public function loggedin(){
        return (bool) $this->session->userdata('loggedin');
    }

    public function hash($string){
        return hash('sha512',$string.config_item('encryption_key'));
    }

    public function get_users_count_all(){
        $users = parent::get();
        return $users;
    }

}