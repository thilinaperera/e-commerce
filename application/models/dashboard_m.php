<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Dashboard_m extends MY_Model{
    protected $_table_name = 'brands';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'name';
    public  $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|max_length[100]|xss_clean'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required|xss_clean'
        ),
    );

    public function get_new(){
        $dashboard = new stdClass();
        $dashboard->name = '';
        $dashboard->description = '';
        return $dashboard;
    }

    public function get_product_issued($first_day = null,$last_day = null ,$id = null , $single = null){

        if($first_day == null){
            $first_day = date('Y-m-01');
        }
        if($last_day == null){
            $last_day  = date('Y-m-d');
        }

        $this->db->select('products_issued_stocks.date_issued, products_issued_stocks.quantity_issued');
        //$this->db->join('products_issued_stocks as pis','products.id = pis.product_id','INNER');
        //$this->db->join('products_issued_stocks as pis','pis.product_id = products.id','INNER');
        $this->db->where('date_issued >=', $first_day);
        $this->db->where('date_issued <=', $last_day);
        $chatData =  $this->db->get('products_issued_stocks');
        return $chatData->result();
    }
    public function get_returns($first_day = null,$last_day = null ,$id = null , $single = null){

        if($first_day == null){
            $first_day = date('Y-m-01');
        }
        if($last_day == null){
            $last_day  = date('Y-m-d');
        }

        $this->db->select('returns.date, returns.quantity');
        //$this->db->join('products_issued_stocks as pis','products.id = pis.product_id','INNER');
        //$this->db->join('products_issued_stocks as pis','pis.product_id = products.id','INNER');
        $this->db->where('date >=', $first_day);
        $this->db->where('date <=', $last_day);
        $chatData =  $this->db->get('returns');
        return $chatData->result();
    }

    // get products with brand name and category name ( join categories,brand tables with products table )
    public function get_cat_and_brand($id = null, $single = false){
        $this->db->select('products.*, b.name as brand, c.name as category');
        $this->db->join('brands as b','products.brand_id = b.id','left');
        $this->db->join('categories as c','products.category_id = c.id','left');
        return $this->db->get();
    }

}