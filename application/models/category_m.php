<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/16/14
 * Time: 2:25 PM
 */
class Category_m extends MY_Model{
    protected $_table_name = 'categories';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'name';
    public  $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|max_length[100]|xss_clean'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required|xss_clean'
        ),
    );

    public function get_new(){
        $category = new stdClass();
        $category->name = '';
        $category->description = '';
        return $category;
    }
}