<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 11:24 AM
 */
class Admin_Controller extends MY_Controller{
    public $data = array();
    function __construct(){
        parent::__construct();
        $this->data['meta_title'] = 'Admin Dashboard';
        $this->load->library('session');
        $this->load->model('user_m');
        $this->load->model('shopping_m');
        $this->data['pending'] = $this->shopping_m->get_all_pending_orders();
        $excet_urls = array(
            'admin/user/login',
            'admin/user/logout'
        );
        if(in_array(uri_string(),$excet_urls)== false){
            if($this->user_m->loggedin()==false){
                redirect('admin/user/login');
            }
            else{
                if($this->user_m->is_member()==true){
                    redirect('member');
                }
            }

        }

    }
}