<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 * Date: 5/14/14
 * Time: 11:24 AM
 */
class Member_Controller extends MY_Controller{
    public $data = array();
    function __construct(){
        parent::__construct();
        $this->data['meta_title'] = 'Ananda foodcity';
        $this->load->library('session');
        $this->load->model('member_m');

        $excet_urls = array(
            'member/login',
            'member/logout',
            'member/register'
        );
        if(in_array(uri_string(),$excet_urls)== false){
            if($this->member_m->loggedin() == false){
                redirect('member/login');
            }
            else{
                if($this->member_m->is_member()==false){
                    redirect('admin/dashboard');
                }
            }
        }

    }
}